package com.example.customlint

import com.android.tools.lint.client.api.IssueRegistry
import com.android.tools.lint.detector.api.CURRENT_API
import com.android.tools.lint.detector.api.Issue
import com.example.customlint.RawDimenDetector.Companion.RAW_DIMEN_ISSUE
import com.example.customlint.XMLFormatDetector.Companion.XML_FORMAT_ISSUE

/**
 *
 * Issue Registry class for all the custom lint rules
 * */
class CustomLintRegistry : IssueRegistry() {

    override val api = CURRENT_API

    override val issues: List<Issue>
        get() = listOf(RAW_DIMEN_ISSUE, XML_FORMAT_ISSUE)
}