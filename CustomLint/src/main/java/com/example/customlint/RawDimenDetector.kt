package com.example.customlint

import com.android.SdkConstants.*
import com.android.tools.lint.detector.api.*
import org.w3c.dom.Attr
import org.w3c.dom.Element
import org.w3c.dom.Node

/**
* This will check for the Raw Dimen if any inside xml files.
* */
class RawDimenDetector : LayoutDetector() {
    companion object {
        private const val MESSAGE = "Hardcoded value should not be used. Replace this with " +
                "base dimen file"
        val RAW_DIMEN_ISSUE = Issue.create(
            id = "RawDimenIssue",
            briefDescription = "Prohibits usage of hardcode value (i.e.,margins and paddings)" +
                    " other than base dimen file.",
            explanation = "Hardcoded value (i.e.,margins and paddings) inside " +
                    "xml should not be used.",
            category = Category.create("Spacing", 5),
            severity = Severity.WARNING,
            implementation = Implementation(
                RawDimenDetector::class.java,
                Scope.RESOURCE_FILE_SCOPE
            )
        )
    }

    /**
     * Enables detector to check all elements
     */
    override fun getApplicableElements() = ALL

    /**
     * Finds issues for the visited element.
     */
    override fun visitElement(context: XmlContext, element: Element) {
        if (element.nodeType == Node.ELEMENT_NODE) {
            checkForSpacingTokens(element, context)
        }
    }

    /**
     * For every element, it looks for [SPACING_ATTRS] and finds if any
     * such attribute is using hardcoded value e.g. 1dp. It does that by checking
     * if the prefix of the attribute value matching with neither of @dimen/ or ?attr/ or ?.
     */
    private fun checkForSpacingTokens(node: Node, context: XmlContext) {
        val valuesToExclude = listOf(DIMEN_PREFIX, ATTR_REF_PREFIX, PREFIX_THEME_REF)
        val numAttrs: Int = node.attributes.length

        for (i in 0 until numAttrs) {
            val attr: Attr = node.attributes.item(i) as Attr
            val attrName: String = attr.nodeName
            val attrValue: String = attr.nodeValue

            if (SPACING_ATTRS.map { PREFIX_ANDROID + it }.contains(attrName)) {
                if (valuesToExclude.none { attrValue.startsWith(it) } &&
                    attrValue != VALUE_ZERO_DP) {
                    val location: Location = context.getLocation(attr)
                    context.report(RAW_DIMEN_ISSUE, attr, location, MESSAGE)
                }
            }
        }
    }
}

val SPACING_ATTRS = listOf(
    "layout_margin",
    "layout_marginBottom",
    "layout_marginEnd",
    "layout_marginHorizontal",
    "layout_marginLeft",
    "layout_marginRight",
    "layout_marginStart",
    "layout_marginTop",
    "layout_marginVertical",
    "padding",
    "paddingBottom",
    "paddingEnd",
    "paddingHorizontal",
    "paddingVertical",
    "paddingLeft",
    "paddingRight",
    "paddingTop",
    "paddingStart"
)