package com.example.customlint

import com.android.tools.lint.detector.api.*
import org.w3c.dom.Attr
import org.w3c.dom.Element
import org.w3c.dom.Node


/**
 * This will raise issue if any xml files are not formatted correctly.
 * */
class XMLFormatDetector : LayoutDetector() {
    companion object {
        private const val MESSAGE = "XML file is not formed correctly. Please do format " +
                "xml file correctly"
        val XML_FORMAT_ISSUE = Issue.create(
            id = "XMLFormatIssue",
            briefDescription = "Prohibits usage of attribute android below app attribute" +
                    " Do format the xml file to avoid the issue.",
            explanation = "Format the XML file according to " +
                    "the given alignment order(i.e. attribute android should be above of app attribute).",
            category = Category.create("Format", 5),
            severity = Severity.WARNING,
            implementation = Implementation(
                XMLFormatDetector::class.java,
                Scope.RESOURCE_FILE_SCOPE
            )
        )
    }

    /**
     * Enables detector to check all elements
     */
    override fun getApplicableElements() = ALL

    /**
     * Finds issues for the visited element.
     */
    override fun visitElement(context: XmlContext, element: Element) {
        if (element.nodeType == Node.ELEMENT_NODE) {
            checkForXmlFormat(element, context)
        }
    }


    /**
     * For every element, it looks for any "app:" attributes if it found
     * then it will check for the next element is "android:" attribute or not,
     * if it found attribute as ":android" then it will raise the ISSUE or else
     * it will go to the next element.
     */
    private fun checkForXmlFormat(node: Node, context: XmlContext) {
        val numAttrs: Int = node.attributes.length

        for (i in 0 until numAttrs) {
            val attr: Attr = node.attributes.item(i) as Attr
            val attrName: String = attr.nodeName
            val location: Location = context.getLocation(attr)
            if (attrName.startsWith("app:")) {
                for (j in i + 1 until numAttrs) {
                    val attrs: Attr = node.attributes.item(j) as Attr
                    val attrNames: String = attrs.nodeName
                    if (attrNames.startsWith("android:")) {
                        context.report(XML_FORMAT_ISSUE, attr, location, MESSAGE)
                    } else {
                        continue
                    }
                }
            }
        }
    }
}