package com.example.biometricauth.model

/**
 * Created by Arjun Satish on 09-09-2020.
 * A model class for passing strings needed to customise the Biometric Dialog
 */
data class PromptStrings(
    var title: String,
    var subtitle: String,
    var description: String,
    var negativeText: String,
    var confirmationRequired :Boolean
)