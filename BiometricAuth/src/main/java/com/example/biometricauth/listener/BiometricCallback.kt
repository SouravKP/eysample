package com.example.biometricauth.listener

import javax.crypto.Cipher

/**
 * Created by Arjun Satish on 09-09-2020.
 * A Callback for catching all the  results from the library
 */

interface BiometricCallback {

    /**
     * No hardware associated with Biometric is present
     */
    fun onHardwareNotPresent()

    /**
     * Hardware is unavailable
     */
    fun onHardwareUnavailable()

    /**
     * Biomeric hardware is available for use
     */
    fun onBiometricAvailable()

    /**
     There is no biometric credentials setup for this account
     */
    fun onBiometricPendingSetup()

    /**
      Success callback for authenticateUsingBiometric() method
     */
    fun onAuthenticationSuccess()

    /**
     Failure callback for authenticateUsingBiometric() method
    */
    fun onAuthenticationFailure()
    /**
     * When the user cancels the biometric authtentication. We can ask the user to login with password
     */
    fun onAuthenticationCancelled()

    /**
     *  When the biometric authentication is success and we got the cipher in success
     *  isEncrypt ---  True -- > Encryption , False --> Decryption
     */
    fun onReadyForEncryption(cipher: Cipher)

}