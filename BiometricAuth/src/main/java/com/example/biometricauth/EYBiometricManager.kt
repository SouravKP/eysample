package com.example.biometricauth

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.example.biometricauth.listener.BiometricCallback
import com.example.biometricauth.model.PromptStrings
import com.example.biometricauth.crytography.CryptographyManager
import com.example.biometricauth.utils.SingletonHolder
import javax.crypto.Cipher

/**
 * Created by Arjun Satish on 09-09-2020.
 * A manager class which handles the following :
 *
1.	Simple Biometric authentication
2.	Encryption using CryptoObject and Biometric to achieve a higher degree of security
3.	Decrypting using cryptographic cipher text
4.  Check if the hardware components are there for biometric to work

 */
class EYBiometricManager private constructor(private val biometricCallback: BiometricCallback) {


    companion object {
        fun getInstance(context: BiometricCallback) =
            SingletonHolder(::EYBiometricManager).getInstance(context)

        val TAG: String = EYBiometricManager::class.java.simpleName

        private var readyToEncrypt: Boolean = false

        private  var cryptographyManager = CryptographyManager()

        /**
         * Any biometric (e.g. fingerprint, iris, or face) on the device that meets or exceeds the
         * requirements for **Class 3** (formerly **Strong**), as defined
         * by the Android CDD.
         */
        var BIOMETRIC_STRONG = BiometricManager.Authenticators.BIOMETRIC_STRONG

        /**
         * Any biometric (e.g. fingerprint, iris, or face) on the device that meets or exceeds the
         * requirements for **Class 2** (formerly **Weak**), as defined by
         * the Android CDD.
         *
         *
         * Note that this is a superset of [.BIOMETRIC_STRONG] and is defined such that
         * `BIOMETRIC_STRONG | BIOMETRIC_WEAK == BIOMETRIC_WEAK`.
         */
        var BIOMETRIC_WEAK = BiometricManager.Authenticators.BIOMETRIC_WEAK

        /**
         * The non-biometric credential used to secure the device (i.e. PIN, pattern, or password).
         * This should typically only be used in combination with a biometric auth type, such as
         * [.BIOMETRIC_WEAK].
         */
        var DEVICE_CREDENTIAL = BiometricManager.Authenticators.DEVICE_CREDENTIAL
    }

    /**
     *  Check if biometric is available and we will get the result in listener
     */
    fun checkIfBiometricsIsAvailable(context: Context) {
        val biometricManager = BiometricManager.from(context)
        when (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                Log.d(TAG, "App can authenticate using biometrics.")
                biometricCallback.onBiometricAvailable()
            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                Log.e(TAG, "No biometric features available on this device.")
                biometricCallback.onHardwareNotPresent()
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                Log.e(TAG, "Biometric features are currently unavailable.")
                biometricCallback.onHardwareUnavailable()
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                Log.e(
                    TAG, "The user hasn't associated " +
                            "any biometric credentials with their account."
                )
                biometricCallback.onBiometricPendingSetup()
            }
            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> {
                TODO()
            }
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> {
                TODO()
            }
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> {
                TODO()
            }
        }
    }

    /**
     * Authenticate using biometric authentication
     */
    fun authenticateUsingBiometric(
        activity: AppCompatActivity,
        authenticator: Int,
        promptStrings: PromptStrings,
    ) {
        val promptInfo = createPromptInfo(promptStrings)
        val biometricPrompt = createBiometricPrompt(activity)
        if (BiometricManager.from(activity)
                .canAuthenticate(authenticator) == BiometricManager.BIOMETRIC_SUCCESS
        ) {
            biometricPrompt.authenticate(promptInfo)
        }
    }


    /**
     * Authenticate using biometric authentication for encryption/Decryption
     */
    fun authenticateUsingBiometricForEncryption(
        activity: AppCompatActivity,
        authenticator: Int,
        promptStrings: PromptStrings,
        cipher: Cipher
    ) {
        val promptInfo = createPromptInfo(promptStrings)
        val biometricPrompt = createBiometricPromptForEncryption(activity)
        if (BiometricManager.from(activity)
                .canAuthenticate(authenticator) == BiometricManager.BIOMETRIC_SUCCESS
        ) {
            biometricPrompt.authenticate(promptInfo,BiometricPrompt.CryptoObject(cipher))
        }
    }


    /**
     *  Create a biometric prompt for simple authentication
     */
    private fun createBiometricPrompt(activity: AppCompatActivity): BiometricPrompt {
        val executor = ContextCompat.getMainExecutor(activity)

        val callback = object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Log.d(TAG, "$errorCode :: $errString")
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                    biometricCallback.onAuthenticationCancelled()
                }
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Log.d(TAG, "Authentication failed for an unknown reason")
                biometricCallback.onAuthenticationFailure()
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                Log.d(TAG, "Authentication was successful")
                 biometricCallback.onAuthenticationSuccess()
            }
        }

        return BiometricPrompt(
            activity,
            executor,
            callback
        )
    }

    /**
     *  Create a biometric prompt for encryption/decryption
     */
    private fun createBiometricPromptForEncryption(activity: AppCompatActivity): BiometricPrompt {
        val executor = ContextCompat.getMainExecutor(activity)

        val callback = object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Log.d(TAG, "$errorCode :: $errString")
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                    biometricCallback.onAuthenticationCancelled()
                }
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Log.d(TAG, "Authentication failed for an unknown reason")
                biometricCallback.onAuthenticationFailure()
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                Log.d(TAG, "Authentication was successful")
                result.cryptoObject?.cipher?.let { biometricCallback.onReadyForEncryption(it) }
            }
        }

        return BiometricPrompt(
            activity,
            executor,
            callback
        )
    }

    /**
     * Create a prompt info
     */
    private fun createPromptInfo(promptString: PromptStrings): BiometricPrompt.PromptInfo {
        return BiometricPrompt.PromptInfo.Builder()
            .setTitle(promptString.title)
            .setSubtitle(promptString.subtitle)
            .setDescription(promptString.description)
            // Authenticate without requiring the user to press a "confirm"
            // button after satisfying the biometric check
            .setConfirmationRequired(promptString.confirmationRequired)
            .setNegativeButtonText(promptString.negativeText)
            .build()
    }


}