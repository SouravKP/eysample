package com.example.biometricauth.crytography

import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import java.nio.charset.Charset
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec

/**
 * Created by Arjun Satish on 09-09-2020.
 */

interface CryptographyManager {

    /**
     * This method first gets or generates an instance of SecretKey and then initializes the Cipher
     * with the key. The secret key uses [ENCRYPT_MODE][Cipher.ENCRYPT_MODE] is used.
     */
    fun getInitializedCipherForEncryption(keyName: String): Cipher

    /**
     * This method first gets or generates an instance of SecretKey and then initializes the Cipher
     * with the key. The secret key uses [DECRYPT_MODE][Cipher.DECRYPT_MODE] is used.
     */
    fun getInitializedCipherForDecryption(keyName: String, initializationVector: ByteArray): Cipher

    /**
     * The Cipher created with [getInitializedCipherForEncryption] is used here
     */
    fun encryptData(plaintext: String, cipher: Cipher): EncryptedData

    /**
     * The Cipher created with [getInitializedCipherForDecryption] is used here
     */
    fun decryptData(ciphertext: ByteArray, cipher: Cipher): String
    /**
     * To get the secret key from Android keystore with keyName
     */
    fun getKeyFromKeystore(keyName: String): SecretKey


}

fun CryptographyManager(): CryptographyManager = CryptographyManagerImpl()

data class EncryptedData(val ciphertext: ByteArray, val initializationVector: ByteArray)

private class CryptographyManagerImpl : CryptographyManager {

    private val KEY_SIZE: Int = 256
    val ANDROID_KEYSTORE = "AndroidKeyStore"
    private val ENCRYPTION_BLOCK_MODE = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        KeyProperties.BLOCK_MODE_GCM
    } else {
        TODO("VERSION.SDK_INT < M")
    }
    private val ENCRYPTION_PADDING = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        KeyProperties.ENCRYPTION_PADDING_NONE
    } else {
        TODO("VERSION.SDK_INT < M")
    }
    private val ENCRYPTION_ALGORITHM = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        KeyProperties.KEY_ALGORITHM_AES
    } else {
        TODO("VERSION.SDK_INT < M")
    }

    override fun getInitializedCipherForEncryption(keyName: String): Cipher {
        val cipher = getCipher()
        val secretKey = getOrCreateSecretKey(keyName)
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        return cipher
    }

    override fun getInitializedCipherForDecryption(keyName: String, initializationVector: ByteArray): Cipher {
        val cipher = getCipher()
        val secretKey = getOrCreateSecretKey(keyName)
        cipher.init(Cipher.DECRYPT_MODE, secretKey, GCMParameterSpec(128, initializationVector))
        return cipher
    }

    override fun encryptData(plaintext: String, cipher: Cipher): EncryptedData {
        val cipherText = cipher.doFinal(plaintext.toByteArray(Charset.forName("UTF-8")))
        return EncryptedData(cipherText,cipher.iv)
    }

    override fun decryptData(ciphertext: ByteArray, cipher: Cipher): String {
        val plainText = cipher.doFinal(ciphertext)
        return String(plainText, Charset.forName("UTF-8"))
    }

    override fun getKeyFromKeystore(keyName: String): SecretKey {
        return getOrCreateSecretKey(keyName)
    }

    private fun getCipher(): Cipher {
        val transformation = "$ENCRYPTION_ALGORITHM/$ENCRYPTION_BLOCK_MODE/$ENCRYPTION_PADDING"
        return Cipher.getInstance(transformation)
    }

     private fun getOrCreateSecretKey(keyName: String, isUserAuthenticationRequired:Boolean= true): SecretKey {
        // If Secretkey was previously created for that keyName, then grab and return it.
        val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
        keyStore.load(null) // Keystore must be loaded before it can be accessed
        keyStore.getKey(keyName, null)?.let { return it as SecretKey }

        // if you reach here, then a new SecretKey must be generated for that keyName
        val paramsBuilder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            KeyGenParameterSpec.Builder(keyName,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        paramsBuilder.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                setEncryptionPaddings(ENCRYPTION_PADDING)
                setKeySize(KEY_SIZE)
                setUserAuthenticationRequired(isUserAuthenticationRequired)
                setBlockModes(ENCRYPTION_BLOCK_MODE)
            }

        }

        val keyGenParams = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            paramsBuilder.build()
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES,
            ANDROID_KEYSTORE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyGenerator.init(keyGenParams)
        }
        return keyGenerator.generateKey()
    }

}