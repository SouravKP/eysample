package com.example.biometricauth.utils

/**
 * This class will help to generate a singleton object. This class follow creator pattern
 * with generic implementation.
 *
 */
open class SingletonHolder<out T : Any, in A>(creator: (A) -> T) {

    // Member variables
    private var creator: ((A) -> T)? = creator

    @Volatile
    private var instance: T? = null

    /**
     * This method will create singleton instance of object. It follow creator pattern with
     * generic implementation.
     *
     * @param arg Generalized parameter.
     * @return Singleton object irrespective of type.
     */
    fun getInstance(arg: A): T {
        val checkInstance = instance
        if (checkInstance != null) {
            return checkInstance
        }
        return synchronized(this) {
            val checkInstanceAgain = instance
            if (checkInstanceAgain != null) {
                checkInstanceAgain
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}