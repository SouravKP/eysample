package com.example.encryption


import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
/**
 * Data class that holds the parameters needed for Encryption and Decryption
 */
data class SecurityParams(var keySpec:SecretKeySpec?, var ivParameterSpec: IvParameterSpec?)