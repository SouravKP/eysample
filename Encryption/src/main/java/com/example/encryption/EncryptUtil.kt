package com.example.encryption

import android.os.Build
import android.security.keystore.KeyProperties
/**
 * This class contains Constants for Encryption and Decryption process
 */
class EncryptUtil {
    companion object {

        const val ENCRYPTION_BLOCK_MODE = KeyProperties.BLOCK_MODE_GCM
        const val ENCRYPTION_PADDING = KeyProperties.ENCRYPTION_PADDING_NONE
        const val ENCRYPTION_ALGORITHM = KeyProperties.KEY_ALGORITHM_AES
        const val PBKDF2 = "PBKDF2WithHmacSHA1"
        const val ITERATION_COUNT = 1000
        const val KEY_LENGTH = 256 //bit
        const val SHA_ALGORITHM = "SHA-1"
        const val SALT =
            "UNi4OZYxCX+1GpALuhGhjG2+t4ny8cCOCxBWf05IrSVQPg7G7SvJkpLvzXoaDtn27+sxt6Yi22WcM/Piq9Ur/+whULvzQTHzSw/+Syfceh18ZhvGcKwpTi8Bs8OxNwgBVhYFSIDqxlh/JYA5I6l6E3sbCHClSN/UQYQTvQ6jBJWaOXLLXe7+0FF8LDD+ju5Am7QsigADcR+LJYPYaiKmOHysix+2EZwso2nTsAV+9IqkRa6hss/bVUfpostsm7qgw7LftTLa7Z656e2AcvOjmbwqGCH/xrCPpmZ5V+EftuLh5NBW3PiUngf897qeRfzeUdIzsux19fPVZjSyYB+4Qw=="

        fun hasMarshmallow() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }
}