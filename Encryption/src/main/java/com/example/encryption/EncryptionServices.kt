package com.example.encryption


import android.util.Base64
import com.example.encryption.EncryptUtil.Companion.hasMarshmallow
import javax.crypto.SecretKey

/**
 * This class provides the api for Encryption and Decryption
 */
class EncryptionServices() {

    private val generateSecret = GenerateSecretKey()


    /**
     * Function which returns the encrypted text.
     * It gets text to be encrypted and
     * SecretKey from Android Keystore
     */
    fun encrypt(data: String, key:SecretKey): String? {
        return if (hasMarshmallow()) {
            ConversionUtil.putMap(encryptWithKey(data,key))
        } else return null
    }

    /**
     * Function that decrypt encrypted text
     * using the SecretKey retrieved from
     * Android Keystore
     */
    fun decrypt(decryptData: String, key:SecretKey): String? {
        return if (hasMarshmallow()) {
            decryptWithKey(ConversionUtil.getMap<DataMap>(decryptData),key)
        } else return null
    }

    /**
     * Function that does the encryption process using Cipher instance
     * by passing Encryption parameters such as SecretKey generated
     * from PBKDF and Initialization Vector
     */
    private fun encryptWithKey(data: String, secretKey:SecretKey): DataMap {
        var encryptParams =
            generateSecret.getEncryptParams(getSha1Password(secretKey.toString()))
        return CipherWrapper(CipherWrapper.TRANSFORMATION).encrypt(data, encryptParams)
    }

    /**
     * Function that does Decryption process using Cipher instance
     * by passing with Decryption parameters such as
     * Secret key generated from PBKDF and Initialization Vector
     */
    private fun decryptWithKey(data: DataMap?, secretKey:SecretKey): String {
        var decryptParams =
            generateSecret.getDecryptParams(
                getSha1Password(secretKey?.toString()),
                data?.iv
            )
        return CipherWrapper(CipherWrapper.TRANSFORMATION).decrypt(
            Base64.encodeToString(
                data?.encrypted,
                Base64.DEFAULT
            ), decryptParams
        )
    }

    /**
     * Function generate SHA-1 Value of SecretKey
     *  from Android Keystore as input
     **/
    private fun getSha1Password(rawString: String?): String? {
        return generateSecret.getSHA1Hash(rawString)
    }
}