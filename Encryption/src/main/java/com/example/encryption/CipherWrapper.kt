package com.example.encryption


import android.util.Base64
import android.util.Log
import com.example.encryption.EncryptUtil.Companion.ENCRYPTION_ALGORITHM
import com.example.encryption.EncryptUtil.Companion.ENCRYPTION_BLOCK_MODE
import com.example.encryption.EncryptUtil.Companion.ENCRYPTION_PADDING
import javax.crypto.Cipher

/**
 * This class wraps [Cipher] class apis with some additional possibilities.
 */
class CipherWrapper(val transformation: String) {


    companion object {
        /**
         * This class wraps [Cipher] class apis with some additional possibilities.
         */
        val TRANSFORMATION = "$ENCRYPTION_ALGORITHM/$ENCRYPTION_BLOCK_MODE/$ENCRYPTION_PADDING"
    }

    private val cipher: Cipher = Cipher.getInstance(transformation)

    /**
     * Function which encrypts the data with Security params
     * which include KeySpec and Initialization Vector(IV)
     * and Returns Encrypted text and IV as ByteArray
     */
    fun encrypt(data: String, encryptParams: SecurityParams): DataMap {
        cipher.init(Cipher.ENCRYPT_MODE, encryptParams.keySpec, encryptParams.ivParameterSpec)
        val cipherText = cipher.doFinal(data.toByteArray())
        var encryptedText = Base64.encodeToString(cipherText, Base64.DEFAULT)
        Log.d("Encrypted data=","{$encryptedText}")
        return DataMap(cipher.iv, cipherText)
    }

    /**
     * Function which decrypt the encrypted data with Security params
     * which include KeySpec and Initialization Vector(IV) and returns Decrypted text
     */
    fun decrypt(data: String, decryptParams: SecurityParams): String {
        cipher.init(Cipher.DECRYPT_MODE, decryptParams.keySpec, decryptParams.ivParameterSpec)
        var plainText: ByteArray = cipher.doFinal(Base64.decode(data, 0))
       var decryptedData = String(plainText)
        Log.d("Decrypted data=","{$decryptedData}")
        return String(plainText)
    }
}
