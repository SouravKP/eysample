package com.example.encryption


/**
 * Data class which stores Initialization Vector(IV) and encrypted text as ByteArray
 */
data class DataMap(var iv: ByteArray, var encrypted: ByteArray)