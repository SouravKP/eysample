package com.example.encryption

import android.os.Build
import androidx.annotation.RequiresApi
import com.google.gson.GsonBuilder
import java.util.*

class ConversionUtil {

    companion object {
        /**
         * Converts byte array input to hex string
         **/
        fun byteToHex(hash: ByteArray): String {
            val formatter = Formatter()
            for (b in hash) {
                formatter.format("%02x", b)
            }
            val result = formatter.toString()
            formatter.close()
            return result
        }

        /**
         * Function returns the object as String using Gson Library
         **/
        @RequiresApi(Build.VERSION_CODES.M)
        fun <T> putMap(`object`: T): String? {
            //Convert object to JSON String.
         return GsonBuilder().create().toJson(`object`)
        }

        /**
         * Used to convert String to corresponding Object type specified.
         **/
        @RequiresApi(Build.VERSION_CODES.M)
        inline fun <reified T> getMap(value: String): T? {
            return GsonBuilder().create().fromJson(value, T::class.java)
        }


    }

}