package com.example.encryption

import android.security.keystore.KeyProperties
import com.example.encryption.EncryptUtil.Companion.SHA_ALGORITHM
import java.io.UnsupportedEncodingException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.spec.InvalidKeySpecException
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
/**
 * This class contains utility methods for key generation
 */
class GenerateSecretKey {
    private var keySpec: SecretKeySpec? = null

    companion object {
        var passPhrase: CharArray? = null
    }
    /**
     * Function returns SecretKeyFactory Instance
     */
    private fun getSecretKeyFactory(): SecretKeyFactory? {
        return SecretKeyFactory.getInstance(EncryptUtil.PBKDF2)
    }
    /**
     * Function returns PBEKeySpec Instance using passPhrase
     *  SALT, ITERATION COUNT and KEY LENGTH
     */
    private fun getPBEKeySpec(
        passphraseOrPin: CharArray?,
        salt: ByteArray?,
        iterationCount: Int,
        length: Int
    ): PBEKeySpec {
        return PBEKeySpec(
            passphraseOrPin,
            salt,
            iterationCount,
            length
        )
    }
    /**
     * Function returns SecretKeySpec Instance for specified Algorithm as input
     */
    private fun getSecretKeySpec(keyBytes: ByteArray?, algorithm: String): SecretKeySpec {
        return SecretKeySpec(keyBytes, algorithm)
    }
    /**
     * Function that generates SecretKey for encryption and decryption
     * generated from PBKDF
     */
    @Throws(NoSuchAlgorithmException::class, InvalidKeySpecException::class)
    fun generateSecretKey(
        passphraseOrPin: CharArray?,
        salt: ByteArray?,
        iterationCount: Int,
        keyLength: Int
    ): SecretKeySpec? {
        val pbKeySpec = getPBEKeySpec(
            passphraseOrPin, salt,
            iterationCount,
            keyLength
        )
        val secretKeyFactory = getSecretKeyFactory()
        val keyBytes = secretKeyFactory?.generateSecret(pbKeySpec)?.encoded
        return getSecretKeySpec(keyBytes, KeyProperties.KEY_ALGORITHM_AES)
    }

    /**
     * Function returns secretkey from PBKDF algorithm with Data
     * to be encrypted/decrypted and other parameters
     */
    private fun getKeyfromPBKDF(data: String?): SecretKeySpec? {
        if (keySpec == null) {
            passPhrase =
                data?.reversed()?.toCharArray()!!
            keySpec = generateSecretKey(
                passPhrase,
                EncryptUtil.SALT.toByteArray(), EncryptUtil.ITERATION_COUNT, EncryptUtil.KEY_LENGTH
            )
        }
        return keySpec
    }
    /**
     * Function returns SHA1-Hash value of the String value passes
     */
    fun getSHA1Hash(string: String?): String? {
        var sha1: String? = ""
        try {
            val crypt: MessageDigest = MessageDigest.getInstance(SHA_ALGORITHM)
            crypt.reset()
            crypt.update(string?.toByteArray())
            sha1 = ConversionUtil.byteToHex(crypt.digest())
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        return if (sha1.isNullOrEmpty()) string else sha1
    }
    /**
     * Function that generates random Initialization Vector(IV)
     */
    private fun getIvParamaterSpec(): IvParameterSpec? {
        val ivRandom = SecureRandom()
        val iv = ByteArray(16)
        ivRandom.nextBytes(iv)
        return IvParameterSpec(iv)
    }
    /**
     * Function returns params needed for encryption
     */
    fun getEncryptParams(password: String?): SecurityParams {
        return SecurityParams(getKeyfromPBKDF(password), getIvParamaterSpec())
    }

    /**
     * Function returns params needed for decryption
     */
    fun getDecryptParams(password: String?, iv: ByteArray?): SecurityParams {
        return SecurityParams(
            getKeyfromPBKDF(password),
            IvParameterSpec(iv)
        )
    }

}