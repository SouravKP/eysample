package com.example.demoapp.di.module

import android.content.Context
import com.example.demoapp.BuildConfig
import com.example.demoapp.DemoApplication
import com.example.demoapp.network.ApiService
import com.example.demoapp.security.KeyStoreWrapper
import com.example.demoapp.utils.common.Authenticator
import com.example.demoapp.utils.common.CustomApiAuthenticator
import com.example.encryption.EncryptionServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.michaelrocks.paranoid.Obfuscate
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
@Obfuscate
object ApplicationModule {

    @Provides
    @BaseUrl
    fun provideBaseUrl() = BuildConfig.BASE_URL


    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext app: Context): DemoApplication {
        return app as DemoApplication
    }

    @Singleton
    @Provides
    fun provideRandomString(): String {
        return "Hilt Testing inside view model...!!!!"
    }

    @Singleton
    @Provides
    fun provideFirebaseAuthenticator(): Authenticator {
        return CustomApiAuthenticator()
    }

    @Singleton
    @Provides
    fun provideApiService(@BaseUrl baseURL: String): ApiService =
        Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)

    /**
     * provide implementations of KeystoreWrapper
     */
    @Provides
    fun provideEncryptionServices(): EncryptionServices = EncryptionServices()

    /**
     * provide EncryptionServices
     */
    @Provides
    fun provideKeyStoreWrapper(): KeyStoreWrapper = KeyStoreWrapper()

}

/**
 *  base url string annotation
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseUrl

