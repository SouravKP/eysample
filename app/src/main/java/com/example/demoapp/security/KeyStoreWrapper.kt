package com.example.demoapp.security

import com.example.biometricauth.crytography.CryptographyManager
import java.security.KeyStore
import javax.crypto.SecretKey


/**
 * This class wraps [KeyStore] class apis with some additional possibilities.
 */
class KeyStoreWrapper{

     var cryptographyManager= CryptographyManager()

    /**
     * Function returns the secret key from Android keystore with Cryptomanager instance
     */
     fun getKeyforEncryption(keyName:String): SecretKey {
        return cryptographyManager.getKeyFromKeystore(keyName)
     }
}

