package com.example.demoapp.constant

import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
object Constants {

    val ANDROID_KEYSTORE_ENCRYPTION = "EncryptKey"

}