package com.example.demoapp.repository

import com.example.demoapp.model.PostListItem
import com.example.demoapp.network.ApiServiceImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * Post list repo to help fetching data from api service
 * */
class PostListRepository @Inject constructor(private val apiServiceImpl: ApiServiceImpl) {

    fun getPost(): Flow<List<PostListItem>> = flow {
        emit(apiServiceImpl.getPost())
    }.flowOn(Dispatchers.IO)
}