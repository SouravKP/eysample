package com.example.demoapp.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.demoapp.R
import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
class BottomSlideAdapter(
    offersListIn: ArrayList<ItemModel>, mCommunicator: FragmentCommunication
) : RecyclerView.Adapter<BottomSlideAdapter.ViewHolder>() {

    private val offersList: ArrayList<ItemModel> = offersListIn
    private var communicator: FragmentCommunication? = null


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_items, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {

        val itemModel: ItemModel = offersList[position]
        holder.txtTitle.text = itemModel.title

        holder.checkBox.isChecked = offersList[position].isSelected
        holder.checkBox.tag = position
        holder.checkBox.setOnClickListener {
            val pos = holder.checkBox.tag as Int
            communicator!!.respond(offersList[pos])
            offersList[pos].isSelected = !offersList[pos].isSelected
        }


    }

    override fun getItemCount(): Int {
        return offersList.size
    }

    fun updateView(itemModel: Int) {
        offersList[itemModel].isSelected = false
        notifyItemChanged(itemModel)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtTitle: TextView = view.findViewById(R.id.txtTitle)
        var checkBox: CheckBox = view.findViewById(R.id.checkBox)

    }

    init {
        communicator = mCommunicator
    }
}