package com.example.demoapp.ui.start

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.demoapp.databinding.ActivityStartScreenBinding
import com.example.demoapp.ui.login.LoginActivity
import io.michaelrocks.paranoid.Obfuscate
import java.util.*

@Obfuscate
class StartScreenActivity : AppCompatActivity() {

    private var _binding: ActivityStartScreenBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityStartScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEnglish.setOnClickListener {
            updateLanguage("en-us")

            callLoginActivity()
        }

        binding.btnArabic.setOnClickListener {
            updateLanguage("ar")
            callLoginActivity()
        }

    }

    private fun callLoginActivity() {
        startActivity(Intent(applicationContext, LoginActivity::class.java))
        finish()
    }


    private fun updateLanguage(language: String) {
        val config = resources.configuration
        val locale = Locale(language)
        Locale.setDefault(locale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            config.setLocale(locale)
        else
            config.locale = locale

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            createConfigurationContext(config)
        resources.updateConfiguration(config, resources.displayMetrics)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}