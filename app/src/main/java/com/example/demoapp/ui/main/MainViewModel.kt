package com.example.demoapp.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.demoapp.utils.common.MainViewValidation
import com.example.demoapp.utils.common.Resource
import com.example.demoapp.utils.common.Validator
import dagger.hilt.android.lifecycle.HiltViewModel
import io.michaelrocks.paranoid.Obfuscate
import javax.inject.Inject

/**
 * A VM for [com.example.demoapp.ui.main.MainActivity].
 */
@HiltViewModel
@Obfuscate
class MainViewModel @Inject constructor(randomString: String) : ViewModel() {

    init {
        println("MainViewModel: $randomString")
    }


    companion object {
        const val TAG = "MainViewModel"
    }


    //Live data variables for corresponding views
    private val validationsList: MutableLiveData<List<MainViewValidation>> by lazy {
        MutableLiveData()
    }


    val firstNameField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val lastNameField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val dobField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val mobileField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    val licenseNumberField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val preNumberField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val spinnerField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    val emailValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(MainViewValidation.Field.FIRSTNAME)
    }

    val passwordValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(MainViewValidation.Field.LASTNAME)
    }

    val dobValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(MainViewValidation.Field.DOB)
    }

    val mobileValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(MainViewValidation.Field.MOBILE)
    }


    val licenseNumberValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(MainViewValidation.Field.LICENSENUMBER)
    }


    val preNumberValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(MainViewValidation.Field.PRENUMBER)
    }


    val spinnerFieldValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(MainViewValidation.Field.SPINNERVALUE)
    }


    //Called Function for all the edittext text change from UI
    fun onFirstNameChange(email: String) = firstNameField.postValue(email)

    fun onLastNameChange(email: String) = lastNameField.postValue(email)

    fun onDOBChange(dob: String) = dobField.postValue(dob)

    fun onNumberChange(number: String) = mobileField.postValue(number)

    fun onLicenseChange(license: String) = licenseNumberField.postValue(license)

    fun onSpinnerValueChange(spinnerValue: String) = spinnerField.postValue(spinnerValue)

    fun onPreTextChange(pretext: String) = preNumberField.postValue(pretext)

    //Filter Function for validating each field from form
    private fun filterValidation(field: MainViewValidation.Field) =
        Transformations.map(validationsList) {
            it.find { validation -> validation.field == field }
                ?.run { return@run this.resource }
                ?: Resource.unknown()
        }

    // validation click event
    fun onMainViewValidate() {
        val firstName = firstNameField.value
        val lastName = lastNameField.value
        val dob = dobField.value
        val mobile = mobileField.value
        val licenseNumber = licenseNumberField.value
        val preNumber = preNumberField.value
        val spinnerValue = spinnerField.value

        val validations = Validator.validateMainFields(
            firstName,
            lastName,
            dob,
            mobile,
            licenseNumber,
            preNumber,
            spinnerValue
        )
        validationsList.postValue(validations)

        if (validations.isNotEmpty()) {
            Log.d(TAG, "Validation Successfully..")
        } else {
            Log.d(TAG, "Validation Failed..")
        }

    }

}