package com.example.demoapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demoapp.databinding.BackdropFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate

@AndroidEntryPoint
@Obfuscate
class BackdropFragment : Fragment(), FragmentCommunication {

    private var _binding: BackdropFragmentBinding? = null
    private val binding get() = _binding!!


    private val sharedViewModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BackdropFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerLayoutManager = LinearLayoutManager(activity)
        binding.slideRV.layoutManager = recyclerLayoutManager

        val dividerItemDecoration = DividerItemDecoration(
            binding.slideRV.context,
            recyclerLayoutManager.orientation
        )
        binding.slideRV.addItemDecoration(dividerItemDecoration)


        val recyclerViewAdapter = getItems().let {
            activity?.let { _ ->
                BottomSlideAdapter(
                    it,
                    this
                )
            }
        }
        binding.slideRV.adapter = recyclerViewAdapter

        sharedViewModel.fragmentRedirectionFromActivity.observe(viewLifecycleOwner, {
            recyclerViewAdapter!!.updateView(it)
        })


    }


    private fun getItems(): ArrayList<ItemModel> {
        val modelList: ArrayList<ItemModel> = ArrayList()
        modelList.add(ItemModel(1, "Item One", isSelected = false))
        modelList.add(ItemModel(2, "Item Two", isSelected = false))
        modelList.add(ItemModel(3, "Item Three", isSelected = false))
        modelList.add(ItemModel(4, "Item Four", isSelected = false))
        modelList.add(ItemModel(5, "Item Five", isSelected = false))
        modelList.add(ItemModel(6, "Item Six", isSelected = false))
        return modelList
    }

    override fun respond(itemModel: ItemModel) {
        sharedViewModel.dataItem.postValue(itemModel)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}

interface FragmentCommunication {
    fun respond(itemModel: ItemModel)
}