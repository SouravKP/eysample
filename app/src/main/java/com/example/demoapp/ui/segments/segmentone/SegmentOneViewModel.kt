package com.example.demoapp.ui.segments.segmentone

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.michaelrocks.paranoid.Obfuscate
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
@Obfuscate
class SegmentOneViewModel @Inject constructor(
    randomString: String
) : ViewModel() {

    init {
        println("SegmentOneViewModel: $randomString")
    }
}