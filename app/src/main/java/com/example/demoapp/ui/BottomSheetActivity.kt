package com.example.demoapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.demoapp.databinding.ActivityBottomSheetBinding
import com.example.demoapp.ui.bottomsheet.BottomSheetOneViewFragment
import com.example.demoapp.ui.bottomsheet.BottomSheetTwoViewFragment
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class BottomSheetActivity : AppCompatActivity() {

    private lateinit var myRoundedBottomSheetDialogOne: BaseBottomSheetDialogFragment


    private var _binding: ActivityBottomSheetBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActivityBottomSheetBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btSelect.setOnClickListener {
            val bottomSheetOneViewFragment = BottomSheetOneViewFragment.newInstance()
            myRoundedBottomSheetDialogOne = BaseBottomSheetDialogFragment.newInstance(
                bottomSheetOneViewFragment,
                bottomSheetOneViewFragment.tag.toString()
            ) { fragment: Fragment ->

                if (fragment is BottomSheetOneViewFragment) {
                    myRoundedBottomSheetDialogOne.disMissDialogFragment()
                    Toast.makeText(applicationContext, "Call back from one", Toast.LENGTH_SHORT)
                        .show()
                }

                if (fragment is BottomSheetTwoViewFragment) {
                    Toast.makeText(applicationContext, "Call back from Two", Toast.LENGTH_SHORT)
                        .show()
                }

            }

            myRoundedBottomSheetDialogOne.show(
                supportFragmentManager,
                myRoundedBottomSheetDialogOne.tag
            )

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
