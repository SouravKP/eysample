package com.example.demoapp.ui.segments.segmentthree

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.demoapp.databinding.SegThreeFragmentBinding
import com.example.demoapp.ui.main.SharedViewModel
import com.example.demoapp.utils.common.Event
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate

@AndroidEntryPoint
@Obfuscate
class SegmentThreeViewFragment : Fragment() {

    private var _binding: SegThreeFragmentBinding? = null
    private val binding get() = _binding!!


    private val sharedViewModel: SharedViewModel by activityViewModels()

    companion object {

        const val TAG: String = "SegmentThreeViewFragment"

        fun newInstance(): SegmentThreeViewFragment {
            val args = Bundle()
            val fragment = SegmentThreeViewFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SegThreeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        println("SegmentThreeViewFragment: $sharedViewModel")

        binding.btnCancel.setOnClickListener {
            sharedViewModel.segmentOneRedirectionFromView.postValue(Event(true))
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}