package com.example.demoapp.ui.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.demoapp.databinding.BottomTwoFragmentBinding
import com.example.demoapp.ui.BaseBottomSheetDialogFragment
import com.example.demoapp.ui.interfaces.UpdateBackStack
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BottomSheetTwoViewFragment : Fragment() {


    private var _binding: BottomTwoFragmentBinding? = null
    private val binding get() = _binding!!

    companion object {

        const val TAG: String = "BottomTwoFragmentBinding"

        fun newInstance(): BottomSheetTwoViewFragment {
            val args = Bundle()
            val fragment = BottomSheetTwoViewFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomTwoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        UpdateBackStack.commonLiveData.observe(viewLifecycleOwner, {
            if (it is BottomSheetTwoViewFragment) {
                Toast.makeText(activity, "call back from two", Toast.LENGTH_SHORT).show()
            }
        })

        binding.notificationsss.setOnClickListener {
            val myRoundedBottomSheetDialogOne = BaseBottomSheetDialogFragment.getInstance()
            val bottomSheetThreeViewFragment = BottomSheetThreeViewFragment.newInstance()
            myRoundedBottomSheetDialogOne.createBottomSheetFragment(
                bottomSheetThreeViewFragment,
                bottomSheetThreeViewFragment.tag.toString()
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}