package com.example.demoapp.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demoapp.utils.common.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import io.michaelrocks.paranoid.Obfuscate
import javax.inject.Inject

/**
 * A VM for [com.example.demoapp.ui.segments.SegmentsActivity].
 */
@HiltViewModel
@Obfuscate
class BottomSheetViewModel @Inject constructor(randomString: String) : ViewModel() {

    init {
        println("SegmentsViewModel: $randomString")
    }

    companion object {
        const val TAG = "TransActivity"
    }

    val bottomOneFragment = MutableLiveData<Event<Boolean>>()
    val bottomTwoFragment = MutableLiveData<Event<Boolean>>()
    val bottomThreeFragment = MutableLiveData<Event<Boolean>>()


    fun onBottomOneClicked() {
        bottomOneFragment.postValue(Event(true))
    }

    fun onBottomTwoClicked() {
        bottomTwoFragment.postValue(Event(true))
    }

    fun onBottomThreeClicked() {
        bottomThreeFragment.postValue(Event(true))
    }

}