package com.example.demoapp.ui.newdynamicview

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.demoapp.utils.common.*
import dagger.hilt.android.lifecycle.HiltViewModel
import io.michaelrocks.paranoid.Obfuscate
import javax.inject.Inject

/**
 * A VM for [com.example.demoapp.ui.newdynamicview.NewDynamicActivity].
 */
@HiltViewModel
@Obfuscate
class NewDynamicViewModel @Inject constructor(randomString: String) : ViewModel() {

    init {
        println("NewDynamicViewModel: $randomString")
    }

    private val validationsList: MutableLiveData<List<NewDynamicViewValidation>> by lazy {
        MutableLiveData()
    }

    private val userNameField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    val userNameValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(NewDynamicViewValidation.Field.USERNAME)
    }

    //Filter Function for validating each field from form
    private fun filterValidation(field: NewDynamicViewValidation.Field) =
        Transformations.map(validationsList) {
            it.find { validation -> validation.field == field }
                ?.run { return@run this.resource }
                ?: Resource.unknown()
        }


    fun editTextDataChange(
        value: String,
        textInputLayoutTag: String
    ) {
        if (textInputLayoutTag.contains(NewTagDetails.USERNAME)) {
            userNameField.postValue(value)
        }
    }


    // validation click event
    fun onNewDynamicViewValidate() {

        val userName = userNameField.value


        val validations = Validator.validateNewDynamicFields(
            userName
        )
        validationsList.postValue(validations)

        if (validations.isNotEmpty() && userName != null
        ) {
            validations.filter { it.resource.status == Status.SUCCESS }.apply {
                if (this.size == validations.size) {
                    Log.d(TAG, "Validation Successfully")
                } else {
                    Log.d(TAG, "Validation Failed")
                }
            }
        } else {
            Log.d(TAG, "Validation Failed")
        }
    }

    companion object {
        private const val TAG = "NewDynamicViewModel"
    }

}

