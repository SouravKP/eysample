package com.example.demoapp.ui.multipleviews

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.demoapp.R
import com.example.demoapp.databinding.ActivityMultipleViewBinding
import com.google.android.flexbox.FlexboxLayout
import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
class MultipleViewActivity : AppCompatActivity() {

    private var _binding: ActivityMultipleViewBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMultipleViewBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.button.setOnClickListener {
            binding.editText.text.isNullOrBlank().apply {
                when {
                    this -> Toast.makeText(
                        this@MultipleViewActivity,
                        "Please Enter a number",
                        Toast.LENGTH_LONG
                    ).show()
                    else -> setTextViewInFlexbox(binding.editText.text.toString().toInt())
                }
            }
        }


    }


    @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
    private fun setTextViewInFlexbox(number: Int) {
        binding.flexboxlayout.removeAllViews()
        for (i in 1..number) {
            val tvNumbers = TextView(this)
            if (i % 2 == 0) {
                tvNumbers.text = " Custom View "
            } else {

                tvNumbers.text = i.toString()
            }
            tvNumbers.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.toFloat())
            tvNumbers.gravity = Gravity.CENTER
            tvNumbers.background = resources.getDrawable(R.drawable.circle_text_flexbox)
            tvNumbers.setTextColor(resources.getColor(android.R.color.white))

            val lpRight = FlexboxLayout.LayoutParams(
                FlexboxLayout.LayoutParams.WRAP_CONTENT,
                FlexboxLayout.LayoutParams.WRAP_CONTENT
            )
            tvNumbers.layoutParams = lpRight
            val lp = tvNumbers.layoutParams as FlexboxLayout.LayoutParams
            lp.setMargins(5, 10, 10, 5)
            tvNumbers.layoutParams = lp
            binding.flexboxlayout.addView(tvNumbers)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}