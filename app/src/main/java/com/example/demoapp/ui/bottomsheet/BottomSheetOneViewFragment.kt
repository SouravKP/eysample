package com.example.demoapp.ui.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.demoapp.databinding.BottomOneFragmentBinding
import com.example.demoapp.ui.BaseBottomSheetDialogFragment
import com.example.demoapp.ui.interfaces.UpdateBackStack
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BottomSheetOneViewFragment : Fragment() {


    private var _binding: BottomOneFragmentBinding? = null
    private val binding get() = _binding!!


    companion object {

        const val TAG: String = "BottomSheetOneViewFragment"

        fun newInstance(): BottomSheetOneViewFragment {
            val args = Bundle()
            val fragment = BottomSheetOneViewFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomOneFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        UpdateBackStack.commonLiveData.observe(viewLifecycleOwner, {
            if (it is BottomSheetOneViewFragment) {
                //dismissMain.disMissDialogFragment()
                Toast.makeText(activity, "call back from one", Toast.LENGTH_SHORT).show()
            }
        })

        binding.notifications.setOnClickListener {

            val myRoundedBottomSheetDialogOne = BaseBottomSheetDialogFragment.getInstance()
            val bottomSheetTwoViewFragment = BottomSheetTwoViewFragment.newInstance()
            myRoundedBottomSheetDialogOne.createBottomSheetFragment(
                bottomSheetTwoViewFragment,
                bottomSheetTwoViewFragment.tag.toString()
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}