package com.example.demoapp.ui.dynamic

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.demoapp.R
import com.example.demoapp.utils.common.*
import dagger.hilt.android.lifecycle.HiltViewModel
import io.michaelrocks.paranoid.Obfuscate
import javax.inject.Inject

/**
 * A VM for [com.example.demoapp.ui.dynamic.DynamicActivity].
 */
@HiltViewModel
@Obfuscate
class DynamicViewModel @Inject constructor(randomString: String) : ViewModel() {

    init {
        println("DynamicViewModel: $randomString")
    }

    companion object {
        private const val TAG = "DynamicViewModel"
    }

    private val _mainForm by lazy {
        MutableLiveData<DynamicFormState>()
    }
    val mainFormState: LiveData<DynamicFormState> by lazy {
        _mainForm
    }

    private val validationsList: MutableLiveData<List<DynamicViewValidation>> by lazy {
        MutableLiveData()
    }

    private val firstNameField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    private val middleNameField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    private val lastNameField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    private val mobileField: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    val firstNameValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(DynamicViewValidation.Field.FIRSTNAME)
    }

    val middleNameValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(DynamicViewValidation.Field.MIDDLENAME)
    }

    val lastNameValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(DynamicViewValidation.Field.LASTNAME)
    }

    val mobileValidation: LiveData<Resource<Int>> by lazy {
        filterValidation(DynamicViewValidation.Field.MOBILE)
    }


    //Filter Function for validating each field from form
    private fun filterValidation(field: DynamicViewValidation.Field) =
        Transformations.map(validationsList) {
            it.find { validation -> validation.field == field }
                ?.run { return@run this.resource }
                ?: Resource.unknown()
        }


    // validation click event
    fun onDynamicViewValidate() {

        val firstname = firstNameField.value
        val middleName = lastNameField.value
        val lastName = lastNameField.value
        val mobile = mobileField.value

        val validations = Validator.validateDynamicFields(
            firstname,
            middleName,
            lastName,
            mobile
        )
        validationsList.postValue(validations)

        if (validations.isNotEmpty() && firstname != null && middleName != null
            && lastName != null && mobile != null
        ) {
            validations.filter { it.resource.status == Status.SUCCESS }.apply {
                if (this.size == validations.size) {
                    Log.d(TAG, "Validation Successfully")
                } else {
                    Log.d(TAG, "Validation Failed")
                }
            }
        } else {
            Log.d(TAG, "Validation Failed")
        }
    }

    //each edit text change event listener
    fun dynamicEditTextDataChange(value: String, textInputLayoutTag: String) {

        if (textInputLayoutTag.contains(TagDetails.FIRSTNAME)) {

            firstNameField.postValue(value)
            if (!isTextFieldValid(value)) {
                _mainForm.value = DynamicFormState(
                    textFieldError = R.string.first_name_field_empty,
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = false
                )

            } else {
                _mainForm.value = DynamicFormState(
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = true
                )

            }

        } else if (textInputLayoutTag.contains(TagDetails.MIDDLE_NAME)) {
            middleNameField.postValue(value)
            if (!isTextFieldValid(value)) {
                _mainForm.value = DynamicFormState(
                    textFieldError = R.string.middle_name_field_empty,
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = false
                )

            } else {
                _mainForm.value = DynamicFormState(
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = true
                )

            }

        } else if (textInputLayoutTag.contains(TagDetails.LASTNAME)) {
            lastNameField.postValue(value)
            if (!isTextFieldValid(value)) {
                _mainForm.value = DynamicFormState(
                    textFieldError = R.string.last_name_field_empty,
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = false
                )

            } else {
                _mainForm.value = DynamicFormState(
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = true
                )
            }
        } else if (textInputLayoutTag.contains(TagDetails.MOBILE)) {
            mobileField.postValue(value)
            if (!isTextFieldValid(value)) {
                _mainForm.value = DynamicFormState(
                    textFieldError = R.string.mobile_field_empty,
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = false
                )
            } else if (isNumberFieldValid(value)) {
                _mainForm.value = DynamicFormState(
                    textFieldError = R.string.number_field_small_length,
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = false
                )
            } else {
                _mainForm.value = DynamicFormState(
                    textInputLayoutTag = textInputLayoutTag,
                    textFieldStatus = true
                )
            }
        }
    }


    // A placeholder value validation check
    private fun isTextFieldValid(textValue: String): Boolean {
        return textValue.isNotBlank()
    }

    private fun isNumberFieldValid(textValue: String): Boolean {
        return textValue.length < 10
    }
}

