package com.example.demoapp.ui.newdynamicview

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import com.example.demoapp.R
import com.example.demoapp.databinding.ActivityNewDynamicBinding
import com.example.demoapp.model.Column
import com.example.demoapp.model.Row
import com.example.demoapp.model.Section
import com.example.demoapp.spinner.MaterialBetterSpinner
import com.example.demoapp.utils.common.*
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate
import org.json.JSONArray
import org.json.JSONObject

@AndroidEntryPoint
@Obfuscate
class NewDynamicActivity : AppCompatActivity(), View.OnClickListener {

    private var _binding: ActivityNewDynamicBinding? = null
    private val binding get() = _binding!!

    private val viewModel: NewDynamicViewModel by viewModels()

    private var newId: Int = 0
    private lateinit var jsonArray: JSONArray
    private var textInputLayoutTag = ""
    private var jsonParsedAL: ArrayList<Section> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityNewDynamicBinding.inflate(layoutInflater)
        setContentView(binding.root)


        // extracting dta from json array..
        val fileInString: String =
            applicationContext.assets.open("LoginDataSource.json").bufferedReader()
                .use { it.readText() }
        val jsonObject = JSONObject(fileInString)

        jsonArray = jsonObject.getJSONArray("sections")

        jsonParsedAL =
            Gson().fromJson(jsonArray.toString(), object : TypeToken<List<Section?>?>() {}.type)

        // text view creation
        addTexts()

        addComponents(jsonParsedAL)

        viewModel.userNameValidation.observe(this@NewDynamicActivity, {
            fieldValidation(it, NewTagDetails.USERNAME)
        })

    }

    /**
     * Each field validation after each field observer with the help of [tagDetails] and [resource]
     * */
    private fun fieldValidation(resource: Resource<Int>, tagDetails: String) {
        for (childView in binding.linearLayout.children) {
            for (childViewCount in 0..binding.linearLayout.childCount) {
                if (binding.linearLayout.getChildAt(childViewCount) is TextInputLayout
                ) {
                    val subChildView =
                        binding.linearLayout.getChildAt(childViewCount) as TextInputLayout

                    if (subChildView.tag.equals(tagDetails) && resource.status == Status.ERROR
                    ) {
                        subChildView.error = resource.data?.run { getString(this) }
                        break
                    } else {
                        if (subChildView.tag.equals(tagDetails)) {
                            subChildView.isErrorEnabled = false
                            break
                        }
                    }
                }
            }
        }
    }

    private fun addComponents(arrayList: ArrayList<Section>) {

        for (i in 0 until arrayList.size) {
            if (arrayList[i].title == "section1") {
                addEditTexts(arrayList[i].rows)
            } else {
                addButtons(arrayList[i].rows)
            }
        }
    }

    private fun addMaterialBetterSpinnerView(element: Column, spinnerItems: ArrayList<String>?) {

        val materialDatePicker = MaterialBetterSpinner(binding.linearLayout.context)
        materialDatePicker.hint = element.placeholder
        materialDatePicker.tag = (newId + 1).toString()
        Util.setTextViewAttributes(materialDatePicker)

        binding.linearLayout.addView(materialDatePicker)

        setSpinner(materialDatePicker.tag as String, spinnerItems)
    }

    // setting the data to spinner (by the help of some hard coded data)
    private fun setSpinner(tag: String, spinnerList: ArrayList<String>?) {

        val arrayAdapter = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_dropdown_item_1line, spinnerList!!.toArray()
        )
        val materialDesignSpinner =
            binding.linearLayout.findViewWithTag(tag) as MaterialBetterSpinner
        materialDesignSpinner.setAdapter(arrayAdapter)
    }

    @SuppressLint("SetTextI18n")
    private fun addTexts() {

        val textView = TextView(binding.linearLayout.context)
        textView.text = "Dynamic Components"
        textView.textSize = 20F
        Util.setTextViewAttributes   (  textView)

        binding.linearLayout.addView(textView)
    }


    private fun addEditTexts(sectionOne: List<Row>) {

        for (i in sectionOne.indices) {

            for (element in sectionOne[i].columns) {

                val inputLayout = TextInputLayout(
                    binding.linearLayout.context,
                    null,
                    R.style.Widget_MaterialComponents_TextInputLayout_OutlinedBox
                )
                inputLayout.setBoxCornerRadii(5f, 5f, 5f, 5f)
                val editText = TextInputEditText(binding.linearLayout.context)


                editText.hint = element.placeholder

                when (element.type) {
                    "normalDropDown" -> {
                        addMaterialBetterSpinnerView(element, element.dropDownListValues)
                    }
                    "Phone Numer" -> {
                        editText.inputType = InputType.TYPE_CLASS_NUMBER
                        editText.filters = arrayOf(LengthFilter(10))
                    }
                    "inlineDatePicker" -> {
                        editText.isFocusable = false
                        editText.requestFocus()
                    }
                    else -> {
                        editText.inputType = InputType.TYPE_CLASS_TEXT
                    }
                }

                if (newId == 0) {
                    newId = 1
                } else {
                    newId += 1
                }

                inputLayout.tag = newId.toString()

                editText.tag = editText.hint as String + element
                inputLayout.addView(editText)
                Util.setEditTextAttributes(inputLayout)
                binding.linearLayout.addView(inputLayout)

                /** end of edit text addition into parent view*/

                editText.apply {
                    afterTextChanged {
                        textInputLayoutTag = inputLayout.tag.toString()

                        viewModel.editTextDataChange(
                            it, textInputLayoutTag
                        )

                    }
                }

                /** end of edit text listeners */
            }
        }
    }

    @SuppressLint("ResourceType")
    private fun addButtons(sectionTwo: List<Row>) {
        val btnTag = Button(binding.linearLayout.context)

        for (i in sectionTwo.indices) {

            for (element in sectionTwo[i].columns) {

                btnTag.text = element.placeholder
                btnTag.tag = (newId + 1).toString()
                Util.setButtonAttributes(btnTag)
                btnTag.setOnClickListener(this)
                binding.linearLayout.addView(btnTag)
            }
        }

    }


    override fun onClick(v: View?) {
        if (v!!.tag.equals("11")) { // for submit button..
            viewModel.onNewDynamicViewValidate()
        }
    }


    /**
     * Extension function to simplify setting an afterTextChanged action to EditText components.
     */
    private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                afterTextChanged.invoke(editable.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}