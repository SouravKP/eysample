package com.example.demoapp.ui.postlist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.demoapp.databinding.PostListItemBinding
import com.example.demoapp.model.PostListItem

/**
 * Post adapter will help [com.example.demoapp.ui.postlist.PostListActivity] for showing the post details
 * */
class PostListAdapter(private var postListItem: ArrayList<PostListItem>) :
    RecyclerView.Adapter<PostListAdapter.PostViewHolder>() {

    private lateinit var postListItemBinding: PostListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        postListItemBinding =
            PostListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostViewHolder(postListItemBinding.root)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        postListItemBinding.txtTask.text = postListItem[position].body
    }

    override fun getItemCount(): Int = postListItem.size

    class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    /**
     * setting the data to recycler view after getting from API by help of [postListItem]
     * */
    fun setData(postListItem: ArrayList<PostListItem>){
        this.postListItem = postListItem
        notifyDataSetChanged()
    }


}