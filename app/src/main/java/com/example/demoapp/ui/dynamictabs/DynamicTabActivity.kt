package com.example.demoapp.ui.dynamictabs

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.demoapp.databinding.ActivityDynamicTabBinding
import com.google.android.material.tabs.TabLayoutMediator
import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
class DynamicTabActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDynamicTabBinding

    private val tabListNames = listOf("Dashboard", "Establishment", "Application","Dashboard", "Establishment",
        "Application","Dashboard", "Establishment", "Application","Application")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDynamicTabBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.pager.adapter = DynamicViewPagerAdapter(this)
        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            tab.text = tabListNames[position]
        }.attach()
    }
}