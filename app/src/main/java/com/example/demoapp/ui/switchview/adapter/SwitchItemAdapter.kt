package com.example.demoapp.ui.switchview.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demoapp.R
import com.example.demoapp.model.SwitchItem
import io.michaelrocks.paranoid.Obfuscate

/**
 * Switching adapter class for list view and grid view
 *
 * */

@Obfuscate
class SwitchItemAdapter(
    items: List<SwitchItem>?, layoutManager: GridLayoutManager?
) : RecyclerView.Adapter<SwitchItemAdapter.ItemViewHolder>() {

    private var mItems: List<SwitchItem>? = items
    private var mLayoutManager: GridLayoutManager? = layoutManager

    override fun getItemViewType(position: Int): Int {
        return if (mLayoutManager?.spanCount == SPAN_COUNT_ONE) {
            VIEW_TYPE_LIST
        } else {
            VIEW_TYPE_GRID
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = if (viewType == VIEW_TYPE_LIST) {
            LayoutInflater.from(parent.context).inflate(R.layout.row_list_item, parent, false)
        } else {
            LayoutInflater.from(parent.context).inflate(R.layout.row_grid_item, parent, false)
        }
        return ItemViewHolder(view, viewType)
    }

    //Binds data on the list
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item: SwitchItem = mItems!![position % 4]
        holder.title?.text = item.title
        holder.iv?.setImageResource(item.imgResId)
        if (getItemViewType(position) == VIEW_TYPE_LIST) {
            holder.info?.text = item.likes.toString() + " likes  ·  " + item.comments + " comments"
        }
    }

    //Returns size of the list
    override fun getItemCount(): Int {
        return 15
    }

    class ItemViewHolder(itemView: View, viewType: Int) : RecyclerView.ViewHolder(itemView) {
        var iv: ImageView? = null
        var title: TextView? = null
        var info: TextView? = null

        init {
            if (viewType == VIEW_TYPE_LIST) {
                iv = itemView.findViewById<View>(R.id.image_big) as ImageView
                title = itemView.findViewById<View>(R.id.title_big) as TextView
                info = itemView.findViewById<View>(R.id.tv_info) as TextView
            } else {
                iv = itemView.findViewById<View>(R.id.image_small) as ImageView
                title = itemView.findViewById<View>(R.id.title_small) as TextView
            }
        }
    }

    companion object {
        const val SPAN_COUNT_ONE = 1 // for list view
        const val SPAN_COUNT_THREE = 3 // for grid view

        const val VIEW_TYPE_GRID = 1
        const val VIEW_TYPE_LIST = 2
    }
}