package com.example.demoapp.ui.dynamictabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.demoapp.databinding.AdapterRepeaterItemBinding
import com.example.demoapp.utils.common.Constants.ARG_OBJECT
import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
class RepeaterObjectFragment : Fragment() {
    private lateinit var binding: AdapterRepeaterItemBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AdapterRepeaterItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(ARG_OBJECT) }?.apply {
            binding.textViewItem.text = getString(ARG_OBJECT) ?: ""
        }
    }
}