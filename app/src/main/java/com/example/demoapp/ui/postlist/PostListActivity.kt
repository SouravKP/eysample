package com.example.demoapp.ui.postlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demoapp.databinding.ActivityPostListBinding
import com.example.demoapp.model.PostListItem
import com.example.demoapp.network.ApiState
import com.example.demoapp.ui.postlist.adapter.PostListAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

/**
 * PostListActivity will show the list of post from the API
 *
 * */

@AndroidEntryPoint
class PostListActivity : AppCompatActivity() {

    private var _binding: ActivityPostListBinding? = null
    private val binding get() = _binding!!

    // Obtain ViewModel from ViewModelProviders
    private val postListViewModel: PostListViewModel by viewModels()
    private lateinit var postListAdapter: PostListAdapter

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityPostListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecyclerView()
        postListViewModel.getPost()

        lifecycleScope.launchWhenStarted {

            postListViewModel._postStateFlow.collect {
                when (it) {
                    is ApiState.Failure -> {
                        binding.recyclerView.isVisible = false
                        binding.progressBar.isVisible = true
                    }
                    ApiState.Loading -> {
                        binding.recyclerView.isVisible = false
                        binding.progressBar.isVisible = true
                    }
                    is ApiState.Success<*> -> {
                        binding.recyclerView.isVisible = true
                        binding.progressBar.isVisible = false
                        postListAdapter.setData(it.data as ArrayList<PostListItem>)
                    }
                    ApiState.Empty -> return@collect
                }
            }
        }

    }

    /**
     * initiate recycler view
     * */
    private fun initRecyclerView() {
        postListAdapter = PostListAdapter(ArrayList())
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@PostListActivity)
            adapter = postListAdapter
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        _binding = null
    }
}