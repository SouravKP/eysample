package com.example.demoapp.ui.segments.segmenttwo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.demoapp.databinding.SegTwoFragmentBinding
import com.example.demoapp.ui.main.SharedViewModel
import com.example.demoapp.utils.common.Event
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate

@AndroidEntryPoint
@Obfuscate
class SegmentTwoViewFragment : Fragment() {

    private var _binding: SegTwoFragmentBinding? = null
    private val binding get() = _binding!!


    private val sharedViewModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SegTwoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    companion object {

        const val TAG: String = "SegmentTwoViewFragment"

        fun newInstance(): SegmentTwoViewFragment {
            val args = Bundle()
            val fragment = SegmentTwoViewFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        println("SegmentTwoViewFragment: $sharedViewModel")

        binding.btnSave.setOnClickListener {
            sharedViewModel.segmentThreeRedirectionFromView.postValue(Event(true))
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}