package com.example.demoapp.ui.segments.segmentone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.example.demoapp.databinding.SegOneFragmentBinding
import com.example.demoapp.ui.main.SharedViewModel
import com.example.demoapp.utils.common.Event
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate

@AndroidEntryPoint
@Obfuscate
class SegmentOneViewFragment : Fragment() {

    private var _binding: SegOneFragmentBinding? = null
    private val binding get() = _binding!!


    private val segmentOneViewModel: SegmentOneViewModel by viewModels()

    private val sharedViewModel: SharedViewModel by activityViewModels()


    companion object {

        const val TAG: String = "SegmentOneViewFragment"

        fun newInstance(): SegmentOneViewFragment {
            val args = Bundle()
            val fragment = SegmentOneViewFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SegOneFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        println("SegmentOneViewFragment: $sharedViewModel")

        segmentOneViewModel

        binding.btnSave.setOnClickListener {
            sharedViewModel.segmentTwoRedirectionFromView.postValue(Event(true))
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}