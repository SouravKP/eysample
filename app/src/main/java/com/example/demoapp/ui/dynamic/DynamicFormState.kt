package com.example.demoapp.ui.dynamic


data class DynamicFormState(
    val textFieldError: Int? = null,
    val textFieldStatus: Boolean? = null,
    val textInputLayoutTag: String? = null,
    val isDataValid: Boolean = false
)