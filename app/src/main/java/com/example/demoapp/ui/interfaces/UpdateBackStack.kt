package com.example.demoapp.ui.interfaces

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData

object UpdateBackStack {

    val commonLiveData = MutableLiveData<Fragment>()
}