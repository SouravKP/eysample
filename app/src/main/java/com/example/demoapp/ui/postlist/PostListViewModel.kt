package com.example.demoapp.ui.postlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.demoapp.network.ApiState
import com.example.demoapp.repository.PostListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject
/**
* A VM for [com.example.demoapp.ui.postlist.PostListActivity]
* */
@HiltViewModel
class PostListViewModel
@Inject
constructor(private val postListRepository: PostListRepository) : ViewModel() {

    private val postStateFlow: MutableStateFlow<ApiState> = MutableStateFlow(ApiState.Empty)

    val _postStateFlow: StateFlow<ApiState> = postStateFlow


    fun getPost() = viewModelScope.launch {
        postStateFlow.value = ApiState.Loading
        postListRepository.getPost().catch { e ->
            postStateFlow.value = ApiState.Failure(e)
        }.collect { data ->
            postStateFlow.value = ApiState.Success(data)
        }
    }
}