package com.example.demoapp.ui.bottomsheet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.demoapp.databinding.BottomThreeFragmentBinding
import com.example.demoapp.ui.BottomSheetNavigationActivity
import com.example.demoapp.ui.interfaces.UpdateBackStack
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BottomSheetThreeViewFragment : Fragment() {


    private var _binding: BottomThreeFragmentBinding? = null
    private val binding get() = _binding!!


    companion object {

        const val TAG: String = "BottomSheetThreeViewFragment"

        fun newInstance(): BottomSheetThreeViewFragment {
            val args = Bundle()
            val fragment = BottomSheetThreeViewFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomThreeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        UpdateBackStack.commonLiveData.observe(viewLifecycleOwner, {
            if (it is BottomSheetThreeViewFragment) {
                Toast.makeText(activity, "call back from three", Toast.LENGTH_SHORT).show()
            }
        })

        binding.help.setOnClickListener {
            startActivity(
                Intent(
                    requireContext(),
                    BottomSheetNavigationActivity::class.java
                )
            )
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}