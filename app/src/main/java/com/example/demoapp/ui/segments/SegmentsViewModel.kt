package com.example.demoapp.ui.segments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demoapp.utils.common.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import io.michaelrocks.paranoid.Obfuscate
import javax.inject.Inject

/**
 * A VM for [com.example.demoapp.ui.segments.SegmentsActivity].
 */
@HiltViewModel
@Obfuscate
class SegmentsViewModel @Inject constructor(randomString: String) : ViewModel() {

    init {
        println("SegmentsViewModel: $randomString")
    }

    companion object {
        const val TAG = "TransActivity"
    }

    val segmentOneFragment = MutableLiveData<Event<Boolean>>()
    val segmentTwoFragment = MutableLiveData<Event<Boolean>>()
    val segmentThreeFragment = MutableLiveData<Event<Boolean>>()


    fun onSegmentOneClicked() {
        segmentOneFragment.postValue(Event(true))
    }

    fun onSegmentTwoClicked() {
        segmentTwoFragment.postValue(Event(true))
    }

    fun onSegmentThreeClicked() {
        segmentThreeFragment.postValue(Event(true))
    }


}