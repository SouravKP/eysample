package com.example.demoapp.ui.dynamic

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.lifecycle.Observer
import com.example.demoapp.R
import com.example.demoapp.databinding.ActivityDynamicBinding
import com.example.demoapp.editText.CustomEditText
import com.example.demoapp.spinner.MaterialBetterSpinner
import com.example.demoapp.utils.common.Resource
import com.example.demoapp.utils.common.Status
import com.example.demoapp.utils.common.TagDetails
import com.example.demoapp.utils.common.Util
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

@Obfuscate
@AndroidEntryPoint
class DynamicActivity : AppCompatActivity(), View.OnClickListener {

    private var spinnerList = arrayOf(
        "Select",
        "Facebook",
        "Twitter",
        "Instagram",
        "WhatsApp"
    )

    companion object {
        const val TAG = "DynamicActivity"
    }

    private lateinit var jsonArray: JSONArray
    private var textInputLayoutTag = ""
    private var editTextInputLayoutTag = ""

    // Obtain ViewModel from ViewModelProviders
    private val viewModel: DynamicViewModel by viewModels()
    private var _binding: ActivityDynamicBinding? = null
    private val binding get() = _binding!!

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityDynamicBinding.inflate(layoutInflater)
        setContentView(binding.root)


        // extracting dta from json array..
        val fileInString: String =
            applicationContext.assets.open("components.json").bufferedReader().use { it.readText() }
        val jsonObject = JSONObject(fileInString)
        jsonArray = jsonObject.getJSONArray("fields")


        // text view creation
        addTexts()

        // edit text creation
        addEditTexts(jsonArray)

        // custom edit text creation
        addCustomEditView()

        // material spinner view creation
        addMaterialBetterSpinnerView()

        // button creation
        addButtons(jsonArray)

        //Observer for all the live data defined inside @DynamicViewModel
        viewModel.mainFormState.observe(this@DynamicActivity, Observer {
            val mainState = it ?: return@Observer

            for (childView in binding.linearLayout.children) {
                for (childViewCount in 0..binding.linearLayout.childCount) {
                    if (binding.linearLayout.getChildAt(childViewCount) is TextInputLayout
                    ) {
                        val subChildView =
                            binding.linearLayout.getChildAt(childViewCount) as TextInputLayout

                        if (mainState.textInputLayoutTag == subChildView.tag &&
                            mainState.textFieldStatus == false
                        ) {
                            subChildView.error =
                                mainState.textFieldError?.run { getString(this) }

                        } else {
                            if (mainState.textInputLayoutTag == subChildView.tag) {
                                subChildView.isErrorEnabled = false
                            }
                        }
                    }
                }
            }


        })

        viewModel.mobileValidation.observe(this@DynamicActivity, {
            fieldValidation(it, TagDetails.MOBILE)
        })

        viewModel.firstNameValidation.observe(this@DynamicActivity, {
            fieldValidation(it, TagDetails.FIRSTNAME)
        })

        viewModel.middleNameValidation.observe(this@DynamicActivity, {
            fieldValidation(it, TagDetails.MIDDLE_NAME)
        })

        viewModel.lastNameValidation.observe(this@DynamicActivity, {
            fieldValidation(it, TagDetails.LASTNAME)
        })


    }

    /**
     * Each field validation after each field observer with the help of [tagDetails] and [resource]
     * */
    private fun fieldValidation(resource: Resource<Int>, tagDetails: String) {
        for (childView in binding.linearLayout.children) {
            for (childViewCount in 0..binding.linearLayout.childCount) {
                if (binding.linearLayout.getChildAt(childViewCount) is TextInputLayout
                ) {
                    val subChildView =
                        binding.linearLayout.getChildAt(childViewCount) as TextInputLayout

                    if (subChildView.tag.equals(tagDetails) && resource.status == Status.ERROR
                    ) {
                        subChildView.error = resource.data?.run { getString(this) }
                        break
                    } else {
                        if (subChildView.tag.equals(tagDetails)) {
                            subChildView.isErrorEnabled = false
                            break
                        }
                    }
                }
            }
        }
    }

    private fun addMaterialBetterSpinnerView() {
        val materialDatePicker = MaterialBetterSpinner(binding.linearLayout.context)
        materialDatePicker.hint = getString(R.string.material_design_spinner)
        materialDatePicker.tag = "android_material_design_spinner"
        Util.setTextViewAttributes(materialDatePicker)

        binding.linearLayout.addView(materialDatePicker)

        setSpinner(materialDatePicker.tag as String)
    }

    @SuppressLint("SetTextI18n")
    private fun addTexts() {

        val textView = TextView(binding.linearLayout.context)
        textView.text = "Dynamic Components"
        textView.textSize = 20F
        Util.setTextViewAttributes(textView)

        binding.linearLayout.addView(textView)
    }

    private fun addCustomEditView() {

        val customTextInputLayout = TextInputLayout(
            this,
            null,
            R.style.Widget_MaterialComponents_TextInputLayout_OutlinedBox
        )

        customTextInputLayout.setBoxCornerRadii(5f, 5f, 5f, 5f)

        val edtCustom = CustomEditText(binding.linearLayout.context)
        edtCustom.hint = "License Number"
        edtCustom.inputType = InputType.TYPE_CLASS_TEXT
        customTextInputLayout.tag = edtCustom.hint
        edtCustom.tag = edtCustom.hint as String + 5
        Util.setEditTextAttributes(customTextInputLayout)
        customTextInputLayout.addView(edtCustom)

        binding.linearLayout.addView(customTextInputLayout)

        edtCustom.apply {
            afterTextChanged {
                textInputLayoutTag = customTextInputLayout.tag.toString()
                editTextInputLayoutTag = edtCustom.tag.toString()

                viewModel.dynamicEditTextDataChange(
                    it, textInputLayoutTag
                )

            }
        }

    }

    private fun addEditTexts(jsonArray: JSONArray) {

        // Date picker init..
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select date")
                .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                .build()

        for (i in 0 until jsonArray.length()) {

            val fieldsJsonObject = jsonArray.getJSONObject(i)
            // added new params if id is not there..
            fieldsJsonObject.put("id", i + 1)

            val inputLayout = TextInputLayout(
                binding.linearLayout.context,
                null,
                R.style.Widget_MaterialComponents_TextInputLayout_OutlinedBox
            )

            inputLayout.setBoxCornerRadii(5f, 5f, 5f, 5f)
            val editText = TextInputEditText(binding.linearLayout.context)

            if (fieldsJsonObject.getString("type")
                    .equals("BUTTON") || fieldsJsonObject.getString("type")
                    .equals("LICENSENUMBER")
            ) continue

            when (fieldsJsonObject.getString("type")) {
                "NUMBER" -> {
                    editText.inputType = InputType.TYPE_CLASS_NUMBER
                    editText.filters = arrayOf(InputFilter.LengthFilter(10))
                }
                else -> {
                    editText.inputType = InputType.TYPE_CLASS_TEXT
                }
            }

            if (fieldsJsonObject.getString("text").equals("DOB")) {
                editText.isFocusable = false
                editText.requestFocus()
            }

            inputLayout.tag = fieldsJsonObject.getString("id")
            editText.hint = fieldsJsonObject.getString("text")
            editText.tag = inputLayout.tag as String + i
            inputLayout.addView(editText)
            Util.setEditTextAttributes(inputLayout)
            binding.linearLayout.addView(inputLayout)



            editText.apply {

                afterTextChanged {
                    textInputLayoutTag = inputLayout.tag.toString()
                    editTextInputLayoutTag = editText.tag.toString()

                    viewModel.dynamicEditTextDataChange(
                        it, textInputLayoutTag
                    )

                }

                setOnClickListener {
                    textInputLayoutTag = inputLayout.tag.toString()
                    editTextInputLayoutTag = editText.tag.toString()

                    if (editTextInputLayoutTag.contains("DOB")) {
                        datePicker.show(supportFragmentManager, editTextInputLayoutTag)
                    }

                    datePicker.addOnPositiveButtonClickListener {
                        binding.linearLayout.findViewWithTag<TextInputEditText>(editTextInputLayoutTag)
                            .setText(
                                datePicker.headerText.toString()
                            )

                    }
                }

            }

        }
    }

    @SuppressLint("ResourceType")
    private fun addButtons(jsonArray: JSONArray) {

        val btnTag = Button(binding.linearLayout.context)

        for (i in 0 until jsonArray.length()) {
            val item = jsonArray.getJSONObject(i)

            if (item.getString("type").equals("BUTTON")) {
                btnTag.text = item.getString("text")
                btnTag.id = item.getInt("id")
                btnTag.tag = "Submit"
                Util.setButtonAttributes(btnTag)
                btnTag.setOnClickListener(this)
                binding.linearLayout.addView(btnTag)
            }

        }
    }


    override fun onClick(v: View?) {
        if (v!!.tag.equals("Submit")) {
            viewModel.onDynamicViewValidate()
        }
    }


    /**
     * Extension function to simplify setting an afterTextChanged action to EditText components.
     */
    private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                afterTextChanged.invoke(editable.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    // setting the data to spinner (by the help of some hard coded data)
    private fun setSpinner(tag: String) {

        val arrayAdapter = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_dropdown_item_1line, spinnerList
        )
        val materialDesignSpinner =
            binding.linearLayout.findViewWithTag(tag) as MaterialBetterSpinner
        materialDesignSpinner.setAdapter(arrayAdapter)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}