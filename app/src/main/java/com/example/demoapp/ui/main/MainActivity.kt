package com.example.demoapp.ui.main

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.demoapp.R
import com.example.demoapp.databinding.ActivityMainBinding
import com.example.demoapp.spinner.MaterialBetterSpinner
import com.example.demoapp.ui.dynamic.DynamicActivity
import com.example.demoapp.ui.multipleviews.MultipleViewActivity
import com.example.demoapp.ui.segments.SegmentsActivity
import com.example.demoapp.utils.common.Status
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.chip.Chip
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate

@AndroidEntryPoint
@Obfuscate
class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!


    private val mainViewModel: MainViewModel by viewModels()

    private val sharedViewModel: SharedViewModel by viewModels()

    private var spinnerList = arrayOf(
        "Facebook",
        "Twitter",
        "Instagram",
        "WhatsApp"
    )

    private var mBottomSheetBehavior: BottomSheetBehavior<View?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.mainViewModel = mainViewModel


        //init spinner
        setSpinner()

        //bottom slid up
        configureBackdrop()


        // Date picker init..
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select date")
                .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                .build()


        // Date picker show event listener..
        findViewById<TextInputEditText>(R.id.edtDatePicker).setOnClickListener {
            datePicker.show(supportFragmentManager, "tag")
        }

        datePicker.addOnPositiveButtonClickListener {
            findViewById<TextInputEditText>(R.id.edtDatePicker).setText(datePicker.headerText)
            Toast.makeText(applicationContext, datePicker.headerText, Toast.LENGTH_SHORT).show()
        }


        //xml edit text change listener
        binding.edtName.apply {
            afterTextChanged {
                mainViewModel.onFirstNameChange(it)
            }
        }

        binding.edtPassword.apply {
            afterTextChanged {
                mainViewModel.onLastNameChange(it)
            }
        }

        binding.edtDatePicker.apply {
            afterTextChanged {
                mainViewModel.onDOBChange(it)
            }
        }


        binding.edtPhoneNumber.apply {
            afterTextChanged {
                mainViewModel.onNumberChange(it)
            }
        }

        binding.edtLicenseNumber.apply {
            afterTextChanged {
                mainViewModel.onLicenseChange(it)
            }
        }

        binding.edtPreText.apply {
            afterTextChanged {
                mainViewModel.onPreTextChange(it)
            }
        }


        binding.androidMaterialDesignSpinner.apply {
            afterTextChanged {
                mainViewModel.onSpinnerValueChange(it)
            }
        }


        //Observer for all the live data defined inside @MainViewModel

        sharedViewModel.dataItem.observe(this, {

            // remove chip from chip group...
            if (!it.isSelected) {
                binding.chipGroup.removeView(binding.chipGroup.findViewWithTag(it.id))
                return@observe
            }

            // new Chip creation...
            val childChip = Chip(this)
            childChip.text = it.title
            childChip.tag = it.id
            childChip.isCloseIconVisible = true
            childChip.isClickable = true
            childChip.isFocusable = true
            childChip.setOnCloseIconClickListener(this)
            binding.chipGroup.addView(childChip)
        })


        mainViewModel.firstNameField.observe(this, {
            if (binding.edtName.text.toString() != it) binding.edtName.setText(it)
        })

        mainViewModel.emailValidation.observe(this, {
            when (it.status) {
                Status.ERROR -> binding.txtInputEmail.error = it.data?.run { getString(this) }
                else -> binding.txtInputEmail.isErrorEnabled = false
            }
        })

        mainViewModel.lastNameField.observe(this, {
            if (binding.edtPassword.text.toString() != it) binding.edtPassword.setText(it)
        })

        mainViewModel.passwordValidation.observe(this, {
            when (it.status) {
                Status.ERROR -> binding.txtInputPassword.error = it.data?.run { getString(this) }
                else -> binding.txtInputPassword.isErrorEnabled = false
            }
        })


        mainViewModel.mobileField.observe(this, {
            if (binding.edtPhoneNumber.text.toString() != it) binding.edtPhoneNumber.setText(it)
        })

        mainViewModel.mobileValidation.observe(this, {
            when (it.status) {
                Status.ERROR -> binding.textInputLayoutMobileNumber.error =
                    it.data?.run { getString(this) }
                else -> binding.textInputLayoutMobileNumber.isErrorEnabled = false
            }
        })

        mainViewModel.licenseNumberField.observe(this, {
            if (binding.edtLicenseNumber.text.toString() != it) binding.edtLicenseNumber.setText(it)
        })

        mainViewModel.licenseNumberValidation.observe(this, {
            when (it.status) {
                Status.ERROR -> binding.textInputLayoutLN.error = it.data?.run { getString(this) }
                else -> binding.textInputLayoutLN.isErrorEnabled = false
            }
        })

        mainViewModel.dobField.observe(this, {
            if (binding.edtDatePicker.text.toString() != it) binding.edtDatePicker.setText(it)
        })

        mainViewModel.dobValidation.observe(this, {
            when (it.status) {
                Status.ERROR -> binding.txtInputDOB.error = it.data?.run { getString(this) }
                else -> binding.txtInputDOB.isErrorEnabled = false
            }
        })

        mainViewModel.preNumberField.observe(this, {
            if (binding.edtPreText.text.toString() != it) binding.edtPreText.setText(it)
        })

        mainViewModel.preNumberValidation.observe(this, {
            when (it.status) {
                Status.ERROR -> binding.textInputLayoutAR.error = it.data?.run { getString(this) }
                else -> binding.textInputLayoutAR.isErrorEnabled = false
            }
        })

        mainViewModel.spinnerField.observe(this, {
            if (binding.androidMaterialDesignSpinner.text.toString() != it)
                binding.androidMaterialDesignSpinner.setText(
                    it
                )
        })

        mainViewModel.spinnerFieldValidation.observe(this, {
            when (it.status) {
                Status.ERROR -> Toast.makeText(
                    applicationContext,
                    R.string.please_select_spinner_data,
                    Toast.LENGTH_SHORT
                ).show()
                else -> return@observe
            }
        })
    }

    private fun configureBackdrop() {

        // Get the fragment reference
        val fragment = supportFragmentManager.findFragmentById(R.id.filter_fragment)

        fragment?.let {
            // Get the BottomSheetBehavior from the fragment view
            BottomSheetBehavior.from(it.requireView()).let { bsb ->
                // Set the initial state of the BottomSheetBehavior to HIDDEN
                bsb.state = BottomSheetBehavior.STATE_HIDDEN

                // Set the trigger that will expand your view
                binding.txtSlid.setOnClickListener {
                    bsb.state = BottomSheetBehavior.STATE_EXPANDED
                }

                // Set the reference into class attribute (will be used latter)
                mBottomSheetBehavior = bsb
            }
        }

    }

    override fun onBackPressed() {
        // With the reference of the BottomSheetBehavior stored
        mBottomSheetBehavior?.let {
            if (it.state == BottomSheetBehavior.STATE_EXPANDED) {
                it.state = BottomSheetBehavior.STATE_COLLAPSED
            } else {
                super.onBackPressed()
            }
        } ?: super.onBackPressed()
    }

    // setting the data to spinner (by the help of some hard coded data)
    private fun setSpinner() {

        val arrayAdapter = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_dropdown_item_1line, spinnerList
        )
        val materialDesignSpinner =
            findViewById<View>(R.id.android_material_design_spinner) as MaterialBetterSpinner
        materialDesignSpinner.setAdapter(arrayAdapter)
    }

    // normal button click event, calling from xml
    fun onSegmentBtnClicked(view: View) {
        startActivity(Intent(applicationContext, SegmentsActivity::class.java))
    }

    // normal button click event, calling from xml
    fun onDynamicViewBtnClicked(view: View) {
        startActivity(Intent(applicationContext, DynamicActivity::class.java))
    }

    // normal button click event, calling from xml
    fun onMultipleViewBtnClicked(view: View) {
        startActivity(Intent(applicationContext, MultipleViewActivity::class.java))
    }

    // normal button click event, calling from xml
    fun onLogoutBtnClicked(view: View) {

        val intent = Intent("custom-action-local-broadcast")
        // on below line we are passing data to our broad cast receiver with key and value pair.
        intent.putExtra("message", "Getting Log off..!!")
        // on below line we are sending our broad cast with intent using broad cast manager.
        LocalBroadcastManager.getInstance(this@MainActivity).sendBroadcast(intent)
        //finishAffinity()
    }

    /**
     * Extension function to simplify setting an afterTextChanged action to EditText components.
     */
    private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                afterTextChanged.invoke(editable.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    override fun onClick(v: View?) {
        val chipViewTag = v!!.tag.toString()
        sharedViewModel.fragmentRedirectionFromActivity.postValue(chipViewTag.toInt() - 1)
        binding.chipGroup.removeView(v)

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}
