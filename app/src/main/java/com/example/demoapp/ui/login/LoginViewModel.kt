package com.example.demoapp.ui.login

import androidx.lifecycle.*
import com.example.demoapp.utils.common.*
import dagger.hilt.android.lifecycle.HiltViewModel
import io.michaelrocks.paranoid.Obfuscate
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * A VM for [com.example.demoapp.ui.login.LoginActivity].
 */
@HiltViewModel
@Obfuscate
class LoginViewModel @Inject constructor(randomString: String, private val auth: Authenticator) :
    ViewModel() {


    init {
        println("LoginViewModel: $randomString")
    }

    //Live data variables for corresponding views
    private val validationsList: MutableLiveData<List<LoginValidation>> by lazy { MutableLiveData() }

    val launchMain: MutableLiveData<Boolean> by lazy { MutableLiveData() }
    val emailField: MutableLiveData<String> by lazy { MutableLiveData() }
    val passwordField: MutableLiveData<String> by lazy { MutableLiveData() }
    val loggingIn: MutableLiveData<Boolean> by lazy { MutableLiveData() }


    val emailValidation: LiveData<Resource<Int>> by lazy { filterValidation(LoginValidation.Field.EMAIL) }
    val passwordValidation: LiveData<Resource<Int>> by lazy { filterValidation(LoginValidation.Field.PASSWORD) }


    //Called Function for all the edittext text change from UI
    fun onEmailChange(email: String) = emailField.postValue(email)

    fun onPasswordChange(email: String) = passwordField.postValue(email)


    //Filter Function for validating each field from form
    private fun filterValidation(field: LoginValidation.Field) =
        Transformations.map(validationsList) {
            it.find { validation -> validation.field == field }
                ?.run { return@run this.resource }
                ?: Resource.unknown()
        }


    //login click event..
     fun onLogin() {
        loggingIn.postValue(true)
        val email = emailField.value
        val password = passwordField.value

        val validations = Validator.validateLoginFields(email, password)
        validationsList.postValue(validations)

        if (validations.isNotEmpty() && email != null && password != null) {
            validations.filter { it.resource.status == Status.SUCCESS }.apply {
                if (this.size == validations.size) {
                    loggingIn.postValue(false)
                    viewModelScope.launch {
                        try {
                            auth.signInWithEmailAndPassword(email, password)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    launchMain.postValue(true)
                } else {
                    loggingIn.postValue(false)
                }
            }
        } else {
            loggingIn.postValue(false)
            launchMain.postValue(false)
        }

    }

}