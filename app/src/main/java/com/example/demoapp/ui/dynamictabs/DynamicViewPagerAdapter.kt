package com.example.demoapp.ui.dynamictabs

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.demoapp.utils.common.Constants.ARG_OBJECT
import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
class DynamicViewPagerAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 10

    override fun createFragment(position: Int): Fragment {
        val fragment = RepeaterObjectFragment()
        fragment.arguments = Bundle().apply {
            putString(ARG_OBJECT, "TAB ${position + 1}")
        }
        return fragment
    }
}