package com.example.demoapp.ui.segments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.demoapp.R
import com.example.demoapp.databinding.ActivitySegmentBinding
import com.example.demoapp.ui.main.SharedViewModel
import com.example.demoapp.ui.segments.segmentone.SegmentOneViewFragment
import com.example.demoapp.ui.segments.segmentthree.SegmentThreeViewFragment
import com.example.demoapp.ui.segments.segmenttwo.SegmentTwoViewFragment
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate

@AndroidEntryPoint
@Obfuscate
class SegmentsActivity : AppCompatActivity() {

    private var _binding: ActivitySegmentBinding? = null
    private val binding get() = _binding!!


    private val tag by lazy { "SegmentsActivity" }

    private var activeFragment: Fragment? = null

    private val segmentViewModel: SegmentsViewModel by viewModels()
    private val sharedViewModel: SharedViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySegmentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        println("SegmentActivity: $sharedViewModel")

        /**snip  */
        // Register mMessageReceiver to receive messages.
        // on below line we are registering our local broadcast manager.
        LocalBroadcastManager.getInstance(this@SegmentsActivity)
            .registerReceiver(broadcastReceiver, IntentFilter("custom-action-local-broadcast"))

        //** snip *//

        //set filled segments directly
        binding.segmentedProgressbar.setCompletedSegments(1) //manually select the segment


        segmentViewModel.segmentOneFragment.observe(this@SegmentsActivity, {
            it.getIfNotHandled()?.run { showSegmentOneViewFragment() }
        })

        segmentViewModel.segmentTwoFragment.observe(this@SegmentsActivity, {
            it.getIfNotHandled()?.run { showSegmentTwoViewFragment() }
        })

        segmentViewModel.segmentThreeFragment.observe(this@SegmentsActivity, {
            it.getIfNotHandled()?.run { showSegmentThreeViewFragment() }
        })

        sharedViewModel.segmentTwoRedirectionFromView.observe(this@SegmentsActivity, {
            it.getIfNotHandled()?.run {
                segmentViewModel.onSegmentTwoClicked()
                binding.segmentedProgressbar
                    .incrementCompletedSegments()
            }
        })

        sharedViewModel.segmentThreeRedirectionFromView.observe(this@SegmentsActivity, {
            it.getIfNotHandled()?.run {
                segmentViewModel.onSegmentThreeClicked()
                binding.segmentedProgressbar
                    .incrementCompletedSegments()
            }
        })

        sharedViewModel.segmentOneRedirectionFromView.observe(this@SegmentsActivity, {
            it.getIfNotHandled()?.run {
                segmentViewModel.onSegmentOneClicked()
                binding.segmentedProgressbar.setCompletedSegments(1)
            }
        })

        segmentViewModel.onSegmentOneClicked()
    }


    private fun showSegmentOneViewFragment() {

        if (activeFragment is SegmentOneViewFragment) return

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment =
            supportFragmentManager.findFragmentByTag(SegmentOneViewFragment.TAG) as SegmentOneViewFragment?

        if (fragment == null) {
            fragment = SegmentOneViewFragment.newInstance()
            fragmentTransaction.add(R.id.fragmentContainer, fragment, SegmentOneViewFragment.TAG)
        } else {
            fragmentTransaction.show(fragment)
        }

        if (activeFragment != null) fragmentTransaction.hide(activeFragment as Fragment)

        fragmentTransaction.commit()

        activeFragment = fragment
    }


    private fun showSegmentTwoViewFragment() {

        if (activeFragment is SegmentTwoViewFragment) return

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment =
            supportFragmentManager.findFragmentByTag(SegmentTwoViewFragment.TAG) as SegmentTwoViewFragment?

        if (fragment == null) {
            fragment = SegmentTwoViewFragment.newInstance()
            fragmentTransaction.add(R.id.fragmentContainer, fragment, SegmentTwoViewFragment.TAG)
        } else {
            fragmentTransaction.show(fragment)
        }

        if (activeFragment != null) fragmentTransaction.hide(activeFragment as Fragment)

        fragmentTransaction.commit()

        activeFragment = fragment

    }

    private fun showSegmentThreeViewFragment() {

        if (activeFragment is SegmentThreeViewFragment) return

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment =
            supportFragmentManager.findFragmentByTag(SegmentThreeViewFragment.TAG) as SegmentThreeViewFragment?

        if (fragment == null) {
            fragment = SegmentThreeViewFragment.newInstance()
            fragmentTransaction.add(R.id.fragmentContainer, fragment, SegmentThreeViewFragment.TAG)
        } else {
            fragmentTransaction.show(fragment)
        }

        if (activeFragment != null) fragmentTransaction.hide(activeFragment as Fragment)

        fragmentTransaction.commit()

        activeFragment = fragment

    }

    override fun onBackPressed() {
        when (activeFragment) {
            is SegmentTwoViewFragment -> {
                segmentViewModel.onSegmentOneClicked()
            }
            is SegmentThreeViewFragment -> {
                segmentViewModel.onSegmentOneClicked()
            }
            else -> {
                super.onBackPressed()
            }
        }
    }


    // on below line we are creating a new broad cast manager.
    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        // we will receive data updates in onReceive method.
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val message = intent.getStringExtra("message")
            // on below line we are updating the data in our text view.
            Log.d(tag, message.toString())
        }
    }


    override fun onDestroy() {
        // Unregister since the activity is not visible
        Log.d(tag, "Finished Segment activity")
        // LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onDestroy()

        _binding = null
    }
}