package com.example.demoapp.ui.main

data class ItemModel(
    val id: Int,
    val title: String,
    var isSelected: Boolean
)