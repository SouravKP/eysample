package com.example.demoapp.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.demoapp.BuildConfig
import com.example.demoapp.constant.Constants.ANDROID_KEYSTORE_ENCRYPTION
import com.example.demoapp.databinding.ActivityLoginBinding
import com.example.demoapp.security.KeyStoreWrapper
import com.example.demoapp.ui.main.MainActivity
import com.example.demoapp.utils.common.Status
import com.example.encryption.EncryptionServices
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate
import java.util.*
import javax.inject.Inject
import kotlin.math.abs

@AndroidEntryPoint
@Obfuscate
class LoginActivity : AppCompatActivity() {

    // Obtain ViewModel from ViewModelProviders
    private val loginViewModel: LoginViewModel by viewModels()

    private var _binding: ActivityLoginBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var keyStoreWrapper: KeyStoreWrapper

    @Inject
    lateinit var encryptionServices: EncryptionServices


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.viewModel = loginViewModel

        onEventErrorMyNameIsASUIAmFromThisThatOkErrorThankYouOnEventErrorMyNameIsASUIAmFromThisThatOkErrorThankYouErrorMyNameIsASUIAmFromThisThatOkErrorThankYou()

        val s =
            myExampleMethodWithAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongName()

        val n = abs(9.8)
        println("The absolute value of 9 is $n")


        //STOPSHIP: do the lint testing...


        //xml edit text change listener
        binding.edtEmail.apply {

            afterTextChanged {
                loginViewModel.onEmailChange(
                    it
                )
            }
        }

        binding.edtPassword.apply {

            afterTextChanged {
                loginViewModel.onPasswordChange(
                    it
                )
            }
        }

        //Observer for all the live data defined inside @LoginViewModel
        loginViewModel.launchMain.observe(
            this,
            Observer {
                if (!it) return@Observer

                val key = keyStoreWrapper.getKeyforEncryption(ANDROID_KEYSTORE_ENCRYPTION)
                val datMapAsString =
                    encryptionServices.encrypt("Sourav", key)

                Toast.makeText(
                    applicationContext,
                    "Encrypt data is -> $datMapAsString",
                    Toast.LENGTH_LONG
                ).show()


                val decryptedData = encryptionServices.decrypt(
                    datMapAsString!!, key
                )

                Toast.makeText(
                    applicationContext,
                    "Decrypt data is -> $decryptedData",
                    Toast.LENGTH_LONG
                ).show()

                /* startActivity(
                     Intent(
                         applicationContext,
                         MainActivity::class.java
                     )
                 )
                 finish()*/
            })

        loginViewModel.emailField.observe(
            this,
            {
                if (binding.edtEmail.text.toString() != it) binding.edtEmail.setText(
                    it
                )
            })

        loginViewModel.emailValidation.observe(
            this,
            {
                when (it.status) {
                    Status.ERROR -> binding.textInputLayoutEmail.error =
                        it.data?.run {
                            getString(
                                this
                            )
                        }
                    else -> binding.textInputLayoutEmail.isErrorEnabled =
                        false
                }
            })

        loginViewModel.passwordField.observe(
            this,
            {
                if (binding.edtPassword.text.toString() != it) binding.edtPassword.setText(
                    it
                )
            })

        loginViewModel.passwordValidation.observe(
            this,
            {
                when (it.status) {
                    Status.ERROR -> binding.txtInputPassword.error =
                        it.data?.run {
                            getString(
                                this
                            )
                        }
                    else -> binding.txtInputPassword.isErrorEnabled =
                        false
                }
            })

    }


    /**
     * Extension function to simplify setting an afterTextChanged action to EditText components.
     */
    private fun EditText.afterTextChanged(
        afterTextChanged: (String) -> Unit
    ) {
        this.addTextChangedListener(
            object :
                TextWatcher {
                override fun afterTextChanged(
                    editable: Editable?
                ) {
                    afterTextChanged.invoke(
                        editable.toString()
                    )
                }

                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
        println(
            "LA: My name is ASU , i am from this that ok error thank you, My name is ASU , i am from this that ok error " +
                    "thank you, My name is ASU , i am from this that ok error thank you, My name is ASU , i am from this" +
                    " that ok error thank you, My name is ASU , i am from this that ok error thank you, My name is ASU , " +
                    "i am from this that ok error thank you, "
        )

        Log.d(
            "TAG TAG TAG TAG TAG TAG TAG TAG TAG TAG TAG ",
            "LA: My name is ASU , i am from this that ok error thank you, My name is ASU ,i am from this that ok error thank you, My name is ASU , i am from this that ok error thank you, " +
                    "My name is ASU , i am from this that ok error thank you, My name is ASU , " +
                    "i am from this that ok error thank you, My name is ASU , i am from this that ok error thank you, "
        )
    }

    private fun myExampleMethodWithAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongName(): () -> Unit =
        { println("Hello World!") }

    private fun onEventErrorMyNameIsASUIAmFromThisThatOkErrorThankYouOnEventErrorMyNameIsASUIAmFromThisThatOkErrorThankYouErrorMyNameIsASUIAmFromThisThatOkErrorThankYou() {

        if (!BuildConfig.DEBUG) {

            Log.d(
                "asd",
                "adsasdasasdasd adasdasd adsadasdasd dsadsadas " +
                        "dsadasd ddsadas"
            )
        }

    }

}
