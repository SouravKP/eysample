package com.example.demoapp.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.example.demoapp.R
import com.example.demoapp.databinding.BaseBottomSheetLayoutBinding
import com.example.demoapp.ui.interfaces.UpdateBackStack
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


/**
 *
 * Base class for creating multiple bottom sheet dialog fragment with help of frame layout and child fragments.
 * */
internal class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    private var _binding: BaseBottomSheetLayoutBinding? = null
    private val binding get() = _binding!!

    companion object {

        const val TAG: String = "BaseBottomSheetFragment"
        private lateinit var baseBottomSheetDialogFragment: BaseBottomSheetDialogFragment
        private lateinit var childFragmentInstance: Fragment
        private lateinit var childFragmentTag: String
        private lateinit var _onBackPressed: (Fragment) -> Unit

        /**
         * create new instance of the base bottom sheet fragment, by passing [newChildFragmentInstance] and [newChildFragmentTag]
         * */
        fun newInstance(
            newChildFragmentInstance: Fragment,
            newChildFragmentTag: String,
            onBackPressed: (Fragment) -> Unit
        ): BaseBottomSheetDialogFragment {
            val args = Bundle()
            baseBottomSheetDialogFragment = BaseBottomSheetDialogFragment()
            baseBottomSheetDialogFragment.arguments = args

            childFragmentInstance = newChildFragmentInstance
            childFragmentTag = newChildFragmentTag
            _onBackPressed = onBackPressed

            return baseBottomSheetDialogFragment
        }

        /**
         * return [baseBottomSheetDialogFragment] instance
         * */
        fun getInstance(): BaseBottomSheetDialogFragment {
            return baseBottomSheetDialogFragment
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BaseBottomSheetLayoutBinding.inflate(inflater, container, false)
        createBottomSheetFragment(
            bottomSheetDialogChildFragment = childFragmentInstance,
            bottomSheetChildTag = childFragmentTag,
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            common.back.setOnClickListener {

                val currentFragment = childFragmentManager.fragments.last()
                _onBackPressed.invoke(currentFragment)

                if (childFragmentManager.backStackEntryCount != 0) {
                    childFragmentManager.popBackStack()
                }

            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)


    override fun getTheme(): Int = R.style.BottomSheetDialogTheme


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        // reset the live data
        UpdateBackStack.commonLiveData.postValue(null)
    }


    /**
     * Dismiss bottom sheet dialog fragment..
     * */
    fun disMissDialogFragment() {
        baseBottomSheetDialogFragment.dismiss()
    }

    /**
     * creating bottom sheet fragment with help of [bottomSheetDialogChildFragment] and [bottomSheetChildTag]
     * */
    fun createBottomSheetFragment(
        bottomSheetDialogChildFragment: Fragment,
        bottomSheetChildTag: String
    ) {

        childFragmentManager.commit {
            replace(
                R.id.base_bottom_sheet_frame_layout,
                bottomSheetDialogChildFragment,
                bottomSheetChildTag
            )
            addToBackStack(bottomSheetChildTag)
        }
    }
}