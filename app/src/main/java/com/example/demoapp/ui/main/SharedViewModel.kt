package com.example.demoapp.ui.main

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demoapp.utils.common.Event
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class SharedViewModel : ViewModel() {

    val segmentTwoRedirectionFromView = MutableLiveData<Event<Boolean>>()

    val segmentOneRedirectionFromView = MutableLiveData<Event<Boolean>>()

    val segmentThreeRedirectionFromView = MutableLiveData<Event<Boolean>>()

    val fragmentRedirectionFromActivity = MutableLiveData<Int>()

    val dataItem = MutableLiveData<ItemModel>()

    val bottomTwoRedirectionFromView = MutableLiveData<Event<Boolean>>()
    val bottomOneRedirectionFromView = MutableLiveData<Event<Boolean>>()

    val bottomThreeRedirectionFromView = MutableLiveData<Event<Boolean>>()


    val commonLiveData: MutableLiveData<Fragment> = MutableLiveData<Fragment>()


}