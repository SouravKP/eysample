package com.example.demoapp.ui.switchview

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.GridLayoutManager
import com.example.demoapp.R
import com.example.demoapp.databinding.ActivitySwitchBinding
import com.example.demoapp.model.SwitchItem
import com.example.demoapp.ui.switchview.adapter.SwitchItemAdapter
import com.example.demoapp.ui.switchview.adapter.SwitchItemAdapter.Companion.SPAN_COUNT_ONE
import com.example.demoapp.ui.switchview.adapter.SwitchItemAdapter.Companion.SPAN_COUNT_THREE
import dagger.hilt.android.AndroidEntryPoint
import io.michaelrocks.paranoid.Obfuscate
import java.util.*

@Obfuscate
@AndroidEntryPoint
class SwitchActivity : AppCompatActivity() {

    private var _binding: ActivitySwitchBinding? = null
    private val binding get() = _binding!!

    lateinit var switchItemAdapter: SwitchItemAdapter
    private lateinit var gridLayoutManager: GridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySwitchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)


        gridLayoutManager = GridLayoutManager(applicationContext, SPAN_COUNT_ONE)


        switchItemAdapter = SwitchItemAdapter(initItemsData(), gridLayoutManager)
        binding.rv.apply {
            adapter = switchItemAdapter
            layoutManager = gridLayoutManager
        }

    }

    private fun switchLayout() {
        if (gridLayoutManager.spanCount == SPAN_COUNT_ONE) {
            gridLayoutManager.spanCount = SPAN_COUNT_THREE
        } else {
            gridLayoutManager.spanCount = SPAN_COUNT_ONE
        }
        switchItemAdapter.notifyItemRangeChanged(0, switchItemAdapter.itemCount)
    }

    /**
     * List and grid item details fun which will return [SwitchItem] type array list
     * */
    private fun initItemsData(): ArrayList<SwitchItem> {
        val items = ArrayList<SwitchItem>(4)
        items.add(SwitchItem(R.drawable.img1, "Image 1", 20, 33))
        items.add(SwitchItem(R.drawable.img2, "Image 2", 10, 54))
        items.add(SwitchItem(R.drawable.img3, "Image 3", 27, 20))
        items.add(SwitchItem(R.drawable.img4, "Image 4", 45, 67))
        return items
    }

    /**
     * Switching the icon in the action bar based on [item] whenever showing List/grid view
     * */
    @Suppress("DEPRECATION")
    private fun switchIcon(item: MenuItem) {
        if (gridLayoutManager.spanCount == SPAN_COUNT_THREE) {
            item.icon = resources.getDrawable(R.drawable.ic_span_3)
        } else {
            item.icon = resources.getDrawable(R.drawable.ic_span_1)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.meun_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_switch_layout) {
            switchLayout()
            switchIcon(item)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}