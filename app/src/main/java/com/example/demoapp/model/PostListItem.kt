package com.example.demoapp.model

/**
 * Post Item data
 * [body] will show the post details.
 * */
class PostListItem(val body: String)