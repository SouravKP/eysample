package com.example.demoapp.model

data class Row(
    val columns: List<Column>
)