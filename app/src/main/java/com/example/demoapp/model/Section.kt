package com.example.demoapp.model

data class Section(
    val title: String,
    val rows: List<Row>
)