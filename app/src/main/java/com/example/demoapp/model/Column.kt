package com.example.demoapp.model

data class Column(
    val type: String,
    val title: String,
    val placeholder: String,
    val value: String,
    val isMandatory: Boolean,
    val isSecureTextEntry: Boolean,
    val keyboardType: KeyboardType,
    val pickerStyle: PickerStyle,
    val validatorType: ValidatorType,
    val dropDownListValues: ArrayList<String>? = null
)