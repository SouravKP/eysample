package com.example.demoapp.model

/**
 * Movie items details
 * */
data class PagingItemResponse(
    val Response: String,
    val Search: List<Movie>,
    val totalResults: String
)