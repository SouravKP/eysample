package com.example.demoapp.model

enum class KeyboardType(val value: String) {
    DefaultKeyBoard("defaultKeyBoard"),
    Email("email"),
    NamePhonePad("namePhonePad");

    companion object {
        fun fromValue(value: String): KeyboardType = when (value) {
            "defaultKeyBoard" -> DefaultKeyBoard
            "email" -> Email
            "namePhonePad" -> NamePhonePad
            else -> throw IllegalArgumentException()
        }
    }
}