package com.example.demoapp.model

data class SwitchItem(
    val imgResId: Int,
    val title: String,
    val likes: Int,
    val comments: Int
)
