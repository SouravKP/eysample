package com.example.demoapp.model

enum class ValidatorType(val value: String) {
    Email("email"),
    None("none"),
    Password("password");

    companion object {
        fun fromValue(value: String): ValidatorType = when (value) {
            "email" -> Email
            "none" -> None
            "password" -> Password
            else -> throw IllegalArgumentException()
        }
    }
}