package com.example.demoapp.model

enum class PickerStyle(val value: String) {
    Inline("inline"),
    Normal("normal");

    companion object {
        fun fromValue(value: String): PickerStyle = when (value) {
            "inline" -> Inline
            "normal" -> Normal
            else -> throw IllegalArgumentException()
        }
    }
}