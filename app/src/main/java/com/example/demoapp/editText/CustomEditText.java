package com.example.demoapp.editText;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;

import java.util.regex.Pattern;


public class CustomEditText extends TextInputEditText {

    public CustomEditText(Context context) {
        super(context);
        addMagic();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        addMagic();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addMagic();
    }

    private void addMagic() {

        // Adding the TextWatcher
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int position, int before, int action) {
                if (position == 2 || position == 6 || position == 13) {
                    if (!s.toString().endsWith("-")) {
                        append("-");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //changeIcon();
            }
        });
        // The input filters
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[0-9\\-]*").matcher(String.valueOf(source)).matches()) {
                    return "";
                }
            }
            return null;
        };
        // Setting the filters
        setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(13)});
    }
}
