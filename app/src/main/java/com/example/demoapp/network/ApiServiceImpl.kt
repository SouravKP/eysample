package com.example.demoapp.network

import com.example.demoapp.model.PostListItem
import javax.inject.Inject

/**
 * Class which implements all generic APIs used across modules
 *
 * property will [apiService]
 */
class ApiServiceImpl @Inject constructor(private val apiService: ApiService) {

    suspend fun getPost(): List<PostListItem> = apiService.getPost()
}