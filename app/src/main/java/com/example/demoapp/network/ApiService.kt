package com.example.demoapp.network

import com.example.demoapp.model.PostListItem
import com.example.demoapp.utils.common.EndPoints
import retrofit2.http.GET

/**
 * Interface which lists out all the APIs used in the application
 *
 */
interface ApiService {

    @GET(EndPoints.POSTS)
    suspend fun getPost(): List<PostListItem>
}