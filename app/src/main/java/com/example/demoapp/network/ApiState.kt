package com.example.demoapp.network

sealed class ApiState {

    /**
     * Loading
     */
    object Loading : ApiState()

    /**
     * Failure response
     */
    class Failure(val msg: Throwable) : ApiState()

    /**
     * success response with body
     */
    class Success<T>(val data: T) : ApiState()

    /**
     * Empty body
     */
    object Empty : ApiState()

}