package com.example.demoapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import io.michaelrocks.paranoid.Obfuscate

@HiltAndroidApp
@Obfuscate
class DemoApplication : Application()