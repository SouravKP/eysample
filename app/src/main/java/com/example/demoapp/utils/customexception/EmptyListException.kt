package com.example.demoapp.utils.customexception

// custom exception class
class EmptyListException(message:String): Exception(message)