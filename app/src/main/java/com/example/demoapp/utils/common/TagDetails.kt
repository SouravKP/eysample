package com.example.demoapp.utils.common

import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
object TagDetails {

    val FIRSTNAME: String by lazy { "1" }
    val LASTNAME: String by lazy { "2" }
    val MIDDLE_NAME: String by lazy { "3" }
    val MOBILE: String by lazy { "4" }
}