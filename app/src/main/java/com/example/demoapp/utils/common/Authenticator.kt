package com.example.demoapp.utils.common

import android.util.Log

interface Authenticator {

    suspend fun signInWithEmailAndPassword(email: String, password: String)
}

class FirebaseAuthenticator : Authenticator {

    override suspend fun signInWithEmailAndPassword(email: String, password: String) {
        Log.d("signInWithEmailAndPasswordFromFirebaseAuthenticator:", "Email is ----------> $email")
    }

}

class CustomApiAuthenticator : Authenticator {

    override suspend fun signInWithEmailAndPassword(email: String, password: String) {
        Log.d("signInWithEmailAndPasswordFromCustomApiAuthenticator:", "Email is -----------> $email")
    }

}