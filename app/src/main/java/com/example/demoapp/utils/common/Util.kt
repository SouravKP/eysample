package com.example.demoapp.utils.common

import android.content.res.Resources
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout
import io.michaelrocks.paranoid.Obfuscate
import kotlin.math.roundToInt

/**
 * For Dynamic view creation
 * */
@Obfuscate
object Util {

    internal fun setEditTextAttributes(editText: TextInputLayout) {
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(
            convertDpToPixel(16f),
            convertDpToPixel(16f),
            convertDpToPixel(16f),
            0
        )

        editText.layoutParams = params
    }

    internal fun setTextViewAttributes(textView: TextView) {
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(
            convertDpToPixel(10f),
            convertDpToPixel(16f),
            convertDpToPixel(16f),
            0
        )

        textView.layoutParams = params
    }

    internal fun setButtonAttributes(button: Button) {
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(
            convertDpToPixel(16f),
            convertDpToPixel(16f),
            convertDpToPixel(16f),
            0
        )
        button.layoutParams = params
    }

    //This function to convert DPs to pixels
    private fun convertDpToPixel(dp: Float): Int {
        val metrics: DisplayMetrics = Resources.getSystem().displayMetrics
        val px = dp * (metrics.densityDpi / 160f)
        return px.roundToInt()
    }


    inline fun <T : Fragment> T.withArgs(argsBuilder: Bundle.() -> Unit): T =
        this.apply {
            arguments = Bundle().apply(argsBuilder)
        }

}