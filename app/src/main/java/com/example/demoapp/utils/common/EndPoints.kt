package com.example.demoapp.utils.common

import io.michaelrocks.paranoid.Obfuscate

/**
 * Constants class which holds end points of all the API url
 *
 */
@Obfuscate
object EndPoints {

    //Posts
    const val POSTS: String = "posts"
}