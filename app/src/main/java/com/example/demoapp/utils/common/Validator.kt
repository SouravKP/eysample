package com.example.demoapp.utils.common

import com.example.demoapp.R
import io.michaelrocks.paranoid.Obfuscate
import java.util.regex.Pattern

@Obfuscate
object Validator {

    private val EMAIL_ADDRESS = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    private const val MIN_PASSWORD_LENGTH = 6

    private const val MIN_MOBILE_LENGTH = 10

    private const val MIN_LICENSE_NUMBER_LENGTH = 13

    fun validateLoginFields(email: String?, password: String?): List<LoginValidation> =
        ArrayList<LoginValidation>().apply {
            when {
                email.isNullOrBlank() ->
                    add(
                        LoginValidation(
                            LoginValidation.Field.EMAIL,
                            Resource.error(R.string.email_field_empty)
                        )
                    )
                !EMAIL_ADDRESS.matcher(email).matches() ->
                    add(
                        LoginValidation(
                            LoginValidation.Field.EMAIL,
                            Resource.error(R.string.email_field_invalid)
                        )
                    )
                else ->
                    add(LoginValidation(LoginValidation.Field.EMAIL, Resource.success()))
            }
            when {
                password.isNullOrBlank() ->
                    add(
                        LoginValidation(
                            LoginValidation.Field.PASSWORD,
                            Resource.error(R.string.password_field_empty)
                        )
                    )
                password?.length!! < MIN_PASSWORD_LENGTH ->
                    add(
                        LoginValidation(
                            LoginValidation.Field.PASSWORD,
                            Resource.error(R.string.password_field_small_length)
                        )
                    )
                else -> add(LoginValidation(LoginValidation.Field.PASSWORD, Resource.success()))
            }
        }


    fun validateMainFields(
        firstName: String?,
        lastName: String?,
        dob: String?,
        mobile: String?,
        licenseNumber: String?,
        preNumber: String?,
        spinnerValue: String?
    ): List<MainViewValidation> =
        ArrayList<MainViewValidation>().apply {
            when {
                firstName.isNullOrBlank() ->
                    add(
                        MainViewValidation(
                            MainViewValidation.Field.FIRSTNAME,
                            Resource.error(R.string.first_name_field_empty)
                        )
                    )
                else ->
                    add(MainViewValidation(MainViewValidation.Field.FIRSTNAME, Resource.success()))
            }
            when {
                lastName.isNullOrBlank() ->
                    add(
                        MainViewValidation(
                            MainViewValidation.Field.LASTNAME,
                            Resource.error(R.string.last_name_field_empty)
                        )
                    )
                else -> add(
                    MainViewValidation(
                        MainViewValidation.Field.LASTNAME,
                        Resource.success()
                    )
                )
            }

            when {
                dob.isNullOrBlank() ->
                    add(
                        MainViewValidation(
                            MainViewValidation.Field.DOB,
                            Resource.error(R.string.dob_field_empty)
                        )
                    )
                else -> add(
                    MainViewValidation(
                        MainViewValidation.Field.DOB,
                        Resource.success()
                    )
                )
            }


            when {
                mobile.isNullOrBlank() ->
                    add(
                        MainViewValidation(
                            MainViewValidation.Field.MOBILE,
                            Resource.error(R.string.mobile_field_empty)
                        )
                    )
                mobile?.length!! < MIN_MOBILE_LENGTH ->
                    add(
                        MainViewValidation(
                            MainViewValidation.Field.MOBILE,
                            Resource.error(R.string.number_field_small_length)
                        )
                    )
                else -> add(
                    MainViewValidation(
                        MainViewValidation.Field.MOBILE,
                        Resource.success()
                    )
                )
            }

            if (licenseNumber != null) {
                when {
                    licenseNumber.isNullOrBlank() ->
                        add(
                            MainViewValidation(
                                MainViewValidation.Field.LICENSENUMBER,
                                Resource.error(R.string.license_field_empty)
                            )
                        )
                    licenseNumber.length < MIN_LICENSE_NUMBER_LENGTH ->
                        add(
                            MainViewValidation(
                                MainViewValidation.Field.LICENSENUMBER,
                                Resource.error(R.string.license_field_small_length)
                            )
                        )
                    else -> add(
                        MainViewValidation(
                            MainViewValidation.Field.LICENSENUMBER,
                            Resource.success()
                        )
                    )
                }
            }

            when {
                preNumber.isNullOrBlank() ->
                    add(
                        MainViewValidation(
                            MainViewValidation.Field.PRENUMBER,
                            Resource.error(R.string.pre_field_empty)
                        )
                    )
                else -> add(
                    MainViewValidation(
                        MainViewValidation.Field.PRENUMBER,
                        Resource.success()
                    )
                )
            }

            when {
                spinnerValue.isNullOrBlank() ->
                    add(
                        MainViewValidation(
                            MainViewValidation.Field.SPINNERVALUE,
                            Resource.error(R.string.spinner_field_empty)
                        )
                    )
                else -> add(
                    MainViewValidation(
                        MainViewValidation.Field.SPINNERVALUE,
                        Resource.success()
                    )
                )
            }


        }


    fun validateDynamicFields(
        firstName: String?,
        middleName: String?,
        lastName: String?,
        mobile: String?
    ): List<DynamicViewValidation> =
        ArrayList<DynamicViewValidation>().apply {
            when {
                firstName.isNullOrBlank() ->
                    add(
                        DynamicViewValidation(
                            DynamicViewValidation.Field.FIRSTNAME,
                            Resource.error(R.string.first_name_field_empty)
                        )
                    )
                else ->
                    add(
                        DynamicViewValidation(
                            DynamicViewValidation.Field.FIRSTNAME,
                            Resource.success()
                        )
                    )
            }
            when {
                middleName.isNullOrBlank() ->
                    add(
                        DynamicViewValidation(
                            DynamicViewValidation.Field.MIDDLENAME,
                            Resource.error(R.string.middle_name_field_empty)
                        )
                    )
                else ->
                    add(
                        DynamicViewValidation(
                            DynamicViewValidation.Field.MIDDLENAME,
                            Resource.success()
                        )
                    )
            }

            when {

                lastName.isNullOrBlank() ->
                    add(
                        DynamicViewValidation(
                            DynamicViewValidation.Field.LASTNAME,
                            Resource.error(R.string.last_name_field_empty)
                        )
                    )
                else ->
                    add(
                        DynamicViewValidation(
                            DynamicViewValidation.Field.LASTNAME,
                            Resource.success()
                        )
                    )

            }

            if (mobile != null) {
                when {
                    mobile.isNullOrBlank() ->
                        add(
                            DynamicViewValidation(
                                DynamicViewValidation.Field.MOBILE,
                                Resource.error(R.string.mobile_field_empty)
                            )
                        )
                    mobile.length < MIN_MOBILE_LENGTH ->
                        add(
                            DynamicViewValidation(
                                DynamicViewValidation.Field.MOBILE,
                                Resource.error(R.string.number_field_small_length)
                            )
                        )
                    else -> add(
                        DynamicViewValidation(
                            DynamicViewValidation.Field.MOBILE,
                            Resource.success()
                        )
                    )
                }
            }
        }


    fun validateNewDynamicFields(
        firstName: String?
    ): List<NewDynamicViewValidation> =
        ArrayList<NewDynamicViewValidation>().apply {
            when {
                firstName.isNullOrBlank() ->
                    add(
                        NewDynamicViewValidation(
                            NewDynamicViewValidation.Field.USERNAME,
                            Resource.error(R.string.first_name_field_empty)
                        )
                    )
                else ->
                    add(
                        NewDynamicViewValidation(
                            NewDynamicViewValidation.Field.USERNAME,
                            Resource.success()
                        )
                    )
            }
        }

}

data class LoginValidation(val field: Field, val resource: Resource<Int>) {

    enum class Field {
        EMAIL,
        PASSWORD
    }
}

data class MainViewValidation(val field: Field, val resource: Resource<Int>) {

    enum class Field {
        FIRSTNAME,
        LASTNAME,
        DOB,
        MOBILE,
        LICENSENUMBER,
        PRENUMBER,
        SPINNERVALUE,
    }
}

data class DynamicViewValidation(val field: Field, val resource: Resource<Int>) {

    enum class Field {
        FIRSTNAME,
        MIDDLENAME,
        LASTNAME,
        MOBILE
    }
}

data class NewDynamicViewValidation(val field: Field, val resource: Resource<Int>) {

    enum class Field {
        USERNAME
    }
}

