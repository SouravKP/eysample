package com.example.demoapp.utils.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    UNKNOWN
}