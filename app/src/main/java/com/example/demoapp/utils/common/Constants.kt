package com.example.demoapp.utils.common

import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
object Constants {

    const val ARG_OBJECT = "object"

    const val API_KEY = "f140f0c"
}