package com.example.demoapp.utils.common

import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
object NewTagDetails {

    val USERNAME: String by lazy { "1" }
}