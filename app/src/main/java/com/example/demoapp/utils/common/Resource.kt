package com.example.demoapp.utils.common

data class Resource<out T> constructor(val status: Status, val data: T?) {

    companion object {
        fun <T> success(data: T? = null): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data
            )
        }

        fun <T> error(data: T? = null): Resource<T> {
            return Resource(
                Status.ERROR,
                data
            )
        }

        fun <T> unknown(data: T? = null): Resource<T> {
            return Resource(
                Status.UNKNOWN,
                data
            )
        }
    }
}